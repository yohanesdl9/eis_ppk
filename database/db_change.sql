INSERT INTO `tb_eis_rf_laporan` (`Kode_Laporan`, `Deskripsi`, `Jenis`, `Class`, `Sumber`, `Request_ID`, `Created_By`, `Created_Date`, `Modified_By`, `Modified_Date`) VALUES ('C-AKD01', 'Pivot Table Mahasiswa', 'custom', 'mahasiswa', NULL, NULL, NULL, '2018-04-26 11:00:20', NULL, NULL);
INSERT INTO `tb_eis_rf_laporan` (`Kode_Laporan`, `Deskripsi`, `Jenis`, `Class`, `Sumber`, `Request_ID`, `Created_By`, `Created_Date`, `Modified_By`, `Modified_Date`) VALUES ('C-AKD02', 'Pivot Table Perwalian', 'custom', 'perwalian', NULL, NULL, NULL, '2018-04-26 11:00:49', NULL, NULL);

DROP TABLE IF EXISTS `tb_akd_rf_kelas_mhs`;
CREATE TABLE `tb_akd_rf_kelas_mhs` (
  `Kelas_Mhs` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Kelas_Deskripsi` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `isAktif` enum('YES','NO') DEFAULT 'YES',
  `Created_By` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Created_Date` datetime DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Modified_Date` datetime DEFAULT NULL,
  KEY `Kelas_Mhs` (`Kelas_Mhs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_akd_rf_kelas_mhs
-- ----------------------------
INSERT INTO `tb_akd_rf_kelas_mhs` VALUES ('R', 'Reguler', 'YES', 'Adrian', '2015-03-23 16:10:24', null, null);
INSERT INTO `tb_akd_rf_kelas_mhs` VALUES ('N', 'Non-Reguler (Professional)', 'NO', 'Adrian', '2015-03-23 16:10:38', null, null);
INSERT INTO `tb_akd_rf_kelas_mhs` VALUES ('K', 'Kerjasama', 'NO', 'Adrian', '2015-03-23 16:10:51', null, null);
INSERT INTO `tb_akd_rf_kelas_mhs` VALUES ('DG', 'Double Degree', 'YES', 'Adrian', '2017-08-09 23:05:45', null, null);

