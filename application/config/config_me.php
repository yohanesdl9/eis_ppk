<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Application
|--------------------------------------------------------------------------
*/
$config['application_id'] = 'EIS';
$config['author'] = 'SISFO';

/*
|--------------------------------------------------------------------------
| Email
|--------------------------------------------------------------------------
*/
$config['email_protocol']='smtp';
//$config['email_smtp_host']='ssl://smtp.gmail.com';
$config['email_smtp_host']='smtp.gmail.com';
$config['email_smtp_user']='';
$config['email_smtp_pass']='';
$config['email_smtp_port']=465;
$config['email_mailtype']='html';
$config['email_charset']='utf-8';
$config['email_newline']="\r\n";

/*
|--------------------------------------------------------------------------
| SMS Gateway
|--------------------------------------------------------------------------
*/
$config['sms_base_url'] = "";
$config['sms_session_file'] = "";
$config['sms_username'] = "";
$config['sms_password'] = "";


?>