<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> Grafik <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="changePivot()" data-tooltip data-placement="top" title="Ubah Grafik">
						<i class="icon-bar-chart"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadPivot()" data-tooltip data-placement="top" title="Download">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body" style="overflow-x: scroll">
				<div class="loader text-center margin-15"><i class="fa fa-refresh fa-spin"></i> &nbsp;Sedang memuat data.... </div>
				<div id="my-pivottable"></div>
				<img drc="" class="img-export hide"/>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/custom/plugins/c3/c3.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/custom/plugins/pivottable/dist/pivot.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>

[/section]

[section name="plugin-js"]
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/c3/c3.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/js/jquery-ui.min.js"></script>
	<script>$.widget.bridge('uitooltip', $.ui.tooltip);</script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/pivottable/dist/pivot.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/pivottable/dist/c3_renderers.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/pivottable/dist/gchart_renderers.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
[/section]


[section name="js"]
	<script type="text/javascript">
	
		$('body').addClass('page-sidebar-closed');
		$('.page-sidebar').addClass('collapse');
		$('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
		
		google.load("visualization", "1", {packages:["corechart", "charteditor"]});
		
		var is_google_chart = <?php echo isset($use_google) ? (string)$use_google : 'false'; ?>;
		var sortAs = $.pivotUtilities.sortAs;
		
		$(function()
		{
			initiate();
		});
		
		function initiate()
		{
			$('.loader').show();
			$('#my-pivottable').hide();
			$('.img-export').attr('src', '');
			
			if (is_google_chart)
			{
				var derivers = $.pivotUtilities.derivers;
				var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.gchart_renderers);
			}
			else
			{
				var derivers = $.pivotUtilities.derivers;
				var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.c3_renderers);
			}

			$.getJSON("<?php echo site_url(PAGE_ID_LAP.'/getDataJSON'); ?>", function(mps) 
			{
				$('.loader').fadeOut(400, function(){
					$("#my-pivottable").pivotUI(mps, {
						renderers: renderers,
						<?php 
							if (isset($cols)) echo "cols : $cols,";
							if (isset($rows)) echo "rows : $rows,";
							if (isset($inclusions)) echo "inclusions : $inclusions,";
							if (isset($exclusions)) echo "exclusions : $exclusions,";
							if (isset($aggregatorName)) echo "aggregatorName : '$aggregatorName',";
							if (isset($vals)) echo "vals : $vals,";
						?>
						rendererName: "<?php echo isset($tipe) ? $tipe : 'Line Chart'; ?>",
						sorters: function(attr) {
							if(attr == "Status_Keu") return sortAs(["Lunas","Belum Lunas","Dispensasi"]);
							if(attr == "Bulan") return sortAs(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']);
							
						},

					});
					$("#my-pivottable").fadeIn(400);
				});
			});
		}
		
		function changePivot()
		{
			is_google_chart = !is_google_chart;
			initiate();
		}

		function downloadPivot()
		{
			var container = $('.pvtRendererArea');
			if(container.find('svg').length)
			{
				if ($('.img-export').attr('src')) window.open($('.img-export').attr('src'));
				else toastr.error('Grafik gagal didownload,<br>harap ubah grafik', 'Download Grafik')
			}
			else if (container.find('table').length)
			{
				name = "Grafik "+$('.caption-subject').first().text().trim()
				container.find('table').table2excel({
					name: name,
					filename: name,
					fileext: '.xls'
				});
			}
		}
	</script>
[/section]