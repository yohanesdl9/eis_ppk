<?php if (!isset($is_ajax) || !$is_ajax) { ?>
[section name="plugin-css"]
	<link href="<?php echo base_url(); ?>assets/pages/css/error.min.css" rel="stylesheet" type="text/css"/>
[/section]

[section name="title"]
	<h1><?php echo PAGE_TITLE && PAGE_TITLE != 'Invalid class name' ? PAGE_TITLE : '404'; ?> &nbsp;<small>Halaman tidak ditemukan</small></h1>
[/section]


<div class="row">
	<div class="col-md-12 page-404">
		<div class="number">404</div>
		<div class="details">
			<h3>Halaman tidak ditemukan !</h3>
			<p>Halaman yang anda cari mungkin masih dalam tahap pengembangan.<br/></p>
		</div>
	</div>

</div>
<?php } else { ?>
<link href="<?php echo base_url(); ?>assets/pages/css/error.min.css" rel="stylesheet" type="text/css"/>
<div class="row">
	<div class="col-md-12 page-404">
		<div class="number">404</div>
		<div class="details">
			<h3 class="text-center">Halaman tidak ditemukan !</h3>
			<p class="text-center">Halaman yang anda cari mungkin masih dalam tahap pengembangan.<br/><br/></p>
		</div>
	</div>
</div>
<?php } ?>