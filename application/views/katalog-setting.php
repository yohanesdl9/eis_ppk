<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" ><?php echo ucwords($jenis).' '.$kode; ?></h4>
</div>
<div class="modal-body">

	<div class="scroll-content">
		<table class="table table-striped">
			<tr>
				<th width="30%">Kode</th>
				<td><?php echo $data->Kode_Laporan; ?></td>
			</tr>
			<tr>
				<th>Nama Laporan</th>
				<td><?php echo $data->Deskripsi; ?></td>
			</tr>
			<tr>
				<th>Jenis</th>
				<td><?php echo ucwords($data->Jenis); ?></td>
			</tr>
			<tr>
				<th>Terakhir diperbarui</th>
				<td><?php echo $data->Modified_Date ? datetime2History($data->Modified_Date) : datetime2History($data->Created_Date); ?></td>
			</tr>
			<?php if ($is_admin) { ?>
			<tr>
				<th colspan="2">Hak Akses</th>
			</tr>
			<tr>
				<td colspan="2">
					<form action="<?php echo site_url(PAGE_ID.'/setting/'.$kode); ?>" method="POST" id="form-hak_akses">
						<table class="table table-striped" id="tb-akses">
							<thead>
								<tr>
									<th>Nama</th>								
									<th width="10%">Lihat</th>
									<th width="10%">Pemilik</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($user)) foreach($user as $i => $row) { ?>
									<tr>
										<td><?php echo $row->Nama; ?> <input type="hidden" name="<?php echo 'data['.$i.'][user]'; ?>" value="<?php echo $row->Username; ?>"></td>
										<td class="text-center"><input type="checkbox" name="<?php echo 'data['.$i.'][view]'; ?>" value="YES" <?php echo $row->isView == 'YES' ? 'checked' : ''; ?>></td>
										<td class="text-center"><input type="checkbox" name="<?php echo 'data['.$i.'][admin]'; ?>" value="YES" <?php echo $row->isAdmin == 'YES' ? 'checked' : ''; ?>></td>
									</tr>
								<?php } ?>
								
							</tbody>
						</table>
					</form>
				</td>
			</tr>
			<?php } else { ?>
			<tr>
				<th>Pemilik</th>
				<td>
					<ul>
						<?php if(is_array($user)) foreach($user as $i => $row) if ($row->isAdmin == 'YES') echo '<li>'.$row->Nama.'</li>'; ?>
					</ul>
				</td>
			</tr>
			<?php } ?>
		</table>
	</div>

</div><!-- /.modal-body -->
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-outline dark">Tutup</button>    
</div>
<?php if ($is_admin) { ?>
<script>
	$('#tb-akses [type="checkbox"]').change(function()
	{
		var form = $('#form-hak_akses');
		$.ajax({
			url : form.attr('action'),
			method : 'POST',
			data : form.serialize(),
			success: function (data)
			{
				if (data != 'ok') toastr.error("Data gagal disimpan !", "Seting Hak Akses");
			},
			error : function (error){
				toastr.error('Koneksi galat : '+error, "Seting Hak Akses")
			}
		});
	})
</script>
<?php } ?>