<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> Katalog <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions tabletool-container">
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<table class="table table-striped table-bordered table-hover" id="datatable-main">
						<thead>
							<tr>
								<th width="10%">Kode</th>
								<th>Deskripsi</th>
								<th width="10%">Link</th>
							</tr>
						</thead>
						<thead class="filter">
							<tr>
								<td><input type="text" class="form-control input-sm"></td>
								<td><input type="text" class="form-control input-sm"></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
[/section]

[section name="plugin-js"]
	<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
[/section]


[section name="js"]
	<script type="text/javascript">
		var datatable = $('#datatable-main').DataTable(
		{
			ajax: "<?php echo site_url(PAGE_ID.'/getDatatable'); ?>",
			serverSide : true,
			pageLength : 50,
			columns: [null, null, {"sortable": false, "class": "text-center"}],
			processing : true,
			lengthChange: false,
			order: [[ 0, "asc" ]],
			lengthMenu : [[50, 100, -1],[50, 100, "All"]],
			responsive: !0,
			/*
			buttons: [{
				extend: "print",
				className: "btn dark btn-outline"
			}, {
				extend: "copy",
				className: "btn red btn-outline"
			}, {
				extend: "pdf",
				className: "btn green btn-outline"
			}, {
				extend: "excel",
				className: "btn yellow btn-outline "
			}, {
				extend: "csv",
				className: "btn purple btn-outline "
			}, {
				extend: "colvis",
				className: "btn dark btn-outline",
				text: "Columns"
			}],
			dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			*/
			dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
		});
		datatable.columns().eq( 0 ).each( function ( colIdx ) 
		{
			$( 'input, select', 'thead.filter tr td:nth-child('+colIdx+')' ).on( 'keyup change', function () {
				datatable.column( (colIdx-1) ).search( this.value ).draw();
			} );
		} );
	
	</script>
[/section]