<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadTable()" data-tooltip data-placement="top" title="Download Excel">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="printTable()" data-tooltip data-placement="top" title="Cetak">
						<i class="icon-printer"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
					<table class="table table-responsive table-bordered table-striped table-hover mytable">
					<thead>
						<tr>
							<th class="text-center" rowspan="2">Tahun Ajaran</th>
							<th class="text-center" rowspan="2">Jumlah Mhs Baru</th>
							<th class="text-center" rowspan="2">Jumlah Total Mhs</th>
							<th class="text-center" rowspan="2">Jumlah Mhs Aktif</th>
							<th class="text-center" rowspan="2">Jumlah Lulusan</th>
							<th class="text-center" colspan="3">IPK Lulusan Reguler</th>
							<th class="text-center" colspan="3">Jumlah Lulusan Reguler dengan IPK:</th>
						</tr>
						<tr>
							<th class="text-center">Min</th>
							<th class="text-center">Rata</th>
							<th class="text-center">Max</th>
							<th class="text-center">< 2.75</th>
							<th class="text-center">2.75 - 3.5</th>
							<th class="text-center">> 3.5</th>
						</tr>
					</thead>
					<thead class="filter">
						<tr>
							<td><input type="text" class="form-control input-sm"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if(is_array($data->data)) foreach($data->data as $row){
						?>
						<tr smt="<?php echo $row->tahun; ?>">
							<td class="text-center"><?php echo $row->Tahun; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_Maba; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_Semua; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_Aktif; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_Lulus; ?></td>
							<td class="text-center"><?php echo number_format($row->Min, 2); ?></td>
							<td class="text-center"><?php echo number_format($row->Rat, 2); ?></td>
							<td class="text-center"><?php echo number_format($row->Max, 2); ?></td>
							<td class="text-center"><?php echo $row->Persen_1; ?></td>
							<td class="text-center"><?php echo $row->Persen_2; ?></td>
							<td class="text-center"><?php echo $row->Persen_3; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
[/section]

[section name="css"]
[/section]

[section name="js"]
	<script type="text/javascript">

		function downloadTable()
		{
			$('.mytable input').each(function(){$(this).attr('extype', $(this).attr('type')).attr('type', 'hidden')});
			$('.mytable').table2excel({
				name: '<?php echo PAGE_TITLE; ?>',
				filename: '<?php echo PAGE_TITLE; ?>',
				fileext: '.xls'
			});
			$('.mytable input').each(function(){$(this).attr('type', $(this).attr('extype')).removeAttr('extype')});
		}

		function printTable()
		{
			$('body').addClass('print');
			window.print();
			$('body').removeClass('print');
		}

		$(document).ready(function()
		{
			$('.filter [name="prodi[]"]').change(function()
			{
				$('.mytable').find('td[ps],th[ps]').hide();
				$.each($(this).val(), function(key, val)
				{
					$('.mytable').find('td[ps="'+val+'"],th[ps="'+val+'"]').show();
				});
			})

			$('.filter [name="periode[]"]').change(function()
			{
				$('.mytable').find('tr[smt]').hide();
				$.each($(this).val(), function(key, val)
				{
					$('.mytable').find('tr[smt="'+val+'"]').show();
				});
			})

			$('.filter [name="periode[]"]').change();
			$('.filter [name="prodi[]"]').change();
			$('.multi-select').selectpicker('render');

			var datatable = $('.mytable').DataTable(
			{
				pageLength : 10,
				lengthChange: true,
				ordering: false,
				responsive: !0,
				dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			}).columns().eq( 0 ).each( function ( colIdx )
			{
				$( 'input, select', 'thead.filter tr td:nth-child('+colIdx+')' ).on( 'keyup change', function () {
					datatable.column( (colIdx-1) ).search( this.value ).draw();
				} );
			} );
		})
	</script>
[/section]
