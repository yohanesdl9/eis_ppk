<?php $period = explode(' ', $periode_smt) ?>
<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadTable()" data-tooltip data-placement="top" title="Download Excel">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="printTable()" data-tooltip data-placement="top" title="Cetak">
						<i class="icon-printer"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row filter margin-b-15 hide-print">
					<div class="form-group">
						<form action="" method="POST">
						<label class="col-md-2 control-label">Periode Smt</label>
						<div class="col-md-4">
							<select class="form-control select" name="periode_smt" onchange="submit()">
								<?php
									foreach($periode as $p)
									{
										$selected = $periode_smt == $p->periode ? 'selected="selected"' : '';
										echo '<option value="'.$p->periode.'" '.$selected.'>'.$p->periode.'</option>';
									}
								?>
							</select>
						</div>
						<label class="col-md-2 control-label">Prodi</label>
						<div class="col-md-4">
							<select class="form-control select" name="prodi" onchange="submit()">
								<?php
									foreach($data->header as $p)
									{
										$selected = $prodi == $p->Kode_Prodi ? 'selected="selected"' : '';
										echo '<option value="'.$p->Kode_Prodi.'" '.$selected.'>'.$p->Kode_Prodi.'</option>';
									}
								?>
							</select>
						</div>
						</form>
					</div>
				</div>
				<table class="table table-responsive table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th rowspan="2" class="text-center">Kode MK</th>
							<th rowspan="2" width="20%" class="text-center">Nama MK</th>
							<th rowspan="2" width="3%" class="text-center">SKS</th>
							<th rowspan="2" width="15%" class="text-center">Nama Dosen</th>
							<th colspan="3" class="text-center">Bobot Penilaian</th>
							<th rowspan="2" width="7%" class="text-center">Jumlah Mhs</th>
							<th colspan="7" class="text-center">Nilai</th>
							<th rowspan="2" class="text-center">% Kelulusan</th>
							<th rowspan="2" class="text-center">Action</th>
						</tr>
						<tr>
							<th class="text-center">Tugas</th>
							<th class="text-center">UTS</th>
							<th class="text-center">UAS</th>
							<th class="text-center">A</th>
							<th class="text-center">B+</th>
							<th class="text-center">B</th>
							<th class="text-center">C+</th>
							<th class="text-center">C</th>
							<th class="text-center">D</th>
							<th class="text-center">E</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if(is_array($data->data)) foreach($data->data as $row){
								$jumlah_lulus = $row->Jumlah_A + $row->Jumlah_BPlus + $row->Jumlah_B + $row->Jumlah_CPlus + $row->Jumlah_C;
						?>
						<tr>
							<td><?php echo $row->Kode_MK; ?></td>
							<td><?php echo $row->Nama_MK; ?></td>
							<td class="text-center"><?php echo $row->sks; ?></td>
							<td><?php echo $row->Nama; ?></td>
							<td class="text-center"><?php echo $row->Bobot_Tugas; ?></td>
							<td class="text-center"><?php echo $row->Bobot_UTS; ?></td>
							<td class="text-center"><?php echo $row->Bobot_UAS; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_Mhs; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_A; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_BPlus; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_B; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_CPlus; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_C; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_D; ?></td>
							<td class="text-center"><?php echo $row->Jumlah_E; ?></td>
							<td class="text-center"><?php echo number_format($jumlah_lulus/$row->Jumlah_Mhs*100, 2); ?></td>
							<th><a href="<?php echo site_url(PAGE_ID."/view/".PAGE_CODE."/form/$period[0]/$period[1]/$row->Kode_Prodi/$row->Kode_MK/$row->nip"); ?>">Detail</a></th>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
[/section]

[section name="css"]
[/section]

[section name="js"]
	<script type="text/javascript">

		function downloadTable()
		{
			$('.mytable input').each(function(){$(this).attr('extype', $(this).attr('type')).attr('type', 'hidden')});
			$('.mytable').table2excel({
				name: '<?php echo PAGE_TITLE; ?>',
				filename: '<?php echo PAGE_TITLE; ?>',
				fileext: '.xls'
			});
			$('.mytable input').each(function(){$(this).attr('type', $(this).attr('extype')).removeAttr('extype')});
		}

		function printTable()
		{
			$('body').addClass('print');
			window.print();
			$('body').removeClass('print');
		}

		$(document).ready(function()
		{
			$('.filter [name="prodi[]"]').change(function()
			{
				$('.mytable').find('td[ps],th[ps]').hide();
				$.each($(this).val(), function(key, val)
				{
					$('.mytable').find('td[ps="'+val+'"],th[ps="'+val+'"]').show();
				});
			})

			$('.filter [name="periode[]"]').change(function()
			{
				$('.mytable').find('tr[smt]').hide();
				$.each($(this).val(), function(key, val)
				{
					$('.mytable').find('tr[smt="'+val+'"]').show();
				});
			})

			$('.filter [name="periode[]"]').change();
			$('.filter [name="prodi[]"]').change();
			$('.multi-select').selectpicker('render');

			var datatable = $('.mytable').DataTable(
			{
				pageLength : -1,
				lengthChange: false,
				responsive: !0,
				dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			}).columns().eq( 0 ).each( function ( colIdx )
			{
				$( 'input, select', 'thead.filter tr td:nth-child('+colIdx+')' ).on( 'keyup change', function () {
					datatable.column( (colIdx-1) ).search( this.value ).draw();
				} );
			} );
		})
	</script>
[/section]
