<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadTable()" data-tooltip data-placement="top" title="Download Excel">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="printTable()" data-tooltip data-placement="top" title="Cetak">
						<i class="icon-printer"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
				
				<div class="row filter margin-b-15 hide-print">
					<div class="form-group">
						<label class="col-md-2 control-label"></label>
						<div class="col-md-4">
						</div>
						<label class="col-md-2 control-label">Prodi</label>
						<div class="col-md-4">
							<select class="form-control multi-select" name="prodi[]" multiple>
								<?php 
								$inclusion = array('TI-S1','MI-D3','DK-S1','SI-S1');
								foreach($data->header as $p) 
								{
									$selected = in_array($p->Kode_Prodi, $inclusion) ? 'selected="selected"' : '';
									echo '<option value="'.$p->Kode_Prodi.'" '.$selected.'>'.$p->Kode_Prodi.'</option>';
								}
								?>
							</select>
						</div>
					</div>

				</div>
			</div>



			<table class="table table-responsive table-striped table-bordered table-hover mytable">
				<thead>
					<tr>
						<th>NRP/Nama</th>
						
						<th ps="Periode" width="30%" class="text-center">Periode</th>
						<th width="10%" class="text-center">kode_prodi</th>
						<th width="10%" class="text-center">check Nilai</th>
					</tr>
				</thead>
				<thead class="filter">
					<tr>
						<td><input type="text" class="form-control input-sm"></td>

					</tr>
				</thead>
				<tbody> 
					<?php 
					if(is_array($data->data)) foreach($data->data as $row){ 
						?>
						<tr smt="<?php echo $row->Kode_Prodi; ?>">
							<td ><?php echo $row->NRP.' '.$row->Nama_Mhs.' ' ?></td>
							<td class ="text-center">
								<?php echo $row->Periode_Sem; ?>
							</td>
							<td class ="text-center" ps="<?php echo $row->Kode_Prodi; ?>" >
								<?php echo $row->Kode_Prodi; ?></td>

								<td class ="text-center" >
									
									<a href="<?php echo site_url(PAGE_ID."/view/".PAGE_CODE."/form/$row->NRP"); ?>">Check Detail</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- END Portlet PORTLET-->
		</div>
	</div>

	[section name="plugin-css"]
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
	[/section]

	[section name="plugin-js"]
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
	[/section]

	[section name="css"]
	[/section]

	[section name="js"]
	<script type="text/javascript">

		function downloadTable()
		{
			$('.mytable input').each(function(){$(this).attr('extype', $(this).attr('type')).attr('type', 'hidden')});
			$('.mytable').table2excel({
				name: '<?php echo PAGE_TITLE; ?>',
				filename: '<?php echo PAGE_TITLE; ?>',
				fileext: '.xls'
			});
			$('.mytable input').each(function(){$(this).attr('type', $(this).attr('extype')).removeAttr('extype')});
		}

		function printTable()
		{
			$('body').addClass('print');
			window.print();
			$('body').removeClass('print');
		}

		$(document).ready(function()
		{


			$('.filter [name="prodi[]"]').change(function()
			{
				$('.mytable').find('tr[smt]').hide();
				$.each($(this).val(), function(key, val)
				{  
					$('.mytable').find('tr[smt="'+val+'"]').show();
				});
			})

			$('.filter [name="prodi[]"]').change();
			//$('.filter [name="Tahun[]"]').change();
			$('.multi-select').selectpicker('render');
			
			var datatable = $('.mytable').DataTable(
			{
				pageLength : 10,
				lengthChange: false,
				order: [[ 0, "desc" ]],
				responsive: !1,
				dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			}).columns().eq( 0 ).each( function ( colIdx ) 
			{
				
				$( 'input, select', 'thead.filter tr td:nth-child('+colIdx+')' ).on( 'keyup change', function () {
					datatable.column( (colIdx-1) ).search( this.value ).draw();
				} );
				
			} );
		})
	</script>
	[/section]