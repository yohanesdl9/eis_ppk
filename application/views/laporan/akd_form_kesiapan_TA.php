<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadTable()" data-tooltip data-placement="top" title="Download Excel">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="printTable()" data-tooltip data-placement="top" title="Cetak">
						<i class="icon-printer"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>

			<table class="table table-responsive table-striped table-bordered table-hover mytable">
				<thead>
					<tr>
						<th>NRP/Nama</th>
						<th>Semester</th>
						<th ps="Periode" width="10%" class="text-center">Periode</th>
						<th ps="IPK" width="7%" class="text-center">IPK</th>
						<th ps="SKS Lulus" width="7%" class="text-center">SKS Lulus</th>
						<th ps="sks_sekarang" width="7%" class="text-center">SKS Sekarang</th>
						<th ps="sks_keseluruhan" width="7%" class="text-center">SKS keseluruhan</th>
						<th width="7%" class="text-center">kode_prodi</th>
					</tr>
				</thead>
				<tbody> 
					<?php 
					if(is_array($data->data2)) foreach($data->data2 as $row1){ 
						?>
						<tr smt="<?php echo $row->Kode_Prodi; ?>">
							<td><?php echo $row1->NRP.' '.$row1->Nama_Mhs.' ' ?></td>
							<td>
								<?php echo $row1->Num_Semester; ?>
							</td>
							<td>
								<?php echo $row1->Periode_Sem; ?>
							</td>
							<td>
								<?php echo $row1->IPK; ?>
							</td>
							<td>
								<?php echo $row1->SKS_Lulus; ?>
							</td>
							<td>
								<?php echo $row1->SKS_Sekarang; ?>
							</td>
							<td>
								<?php echo $row1->SKS_Keseluruhan; ?>
							</td>
							<td class ="text-center" ps="<?php echo $row->Kode_Prodi; ?>" >
								<?php echo $row1->Kode_Prodi; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
			
			<table class="table table-responsive table-striped table-bordered table-hover mytable">
				<thead>
					<tr>
						<th width="7%">Kode_Prodi / Nama MHS</th>
						<th width="7%">Kode_MK</th>
						<th ps="Nama_MK" width="10%" class="text-center">Nama Matakuliah</th>
						<th ps="Nilai" width="7%" class="text-center">Nilai</th>
						<th ps="Tahun" width="7%" class="text-center">Tahun</th>
						<th ps="Periode_Semester" width="7%" class="text-center">Periode Semester</th>
						<th ps="Ambil_ke" width="7%" class="text-center">ambil Ke</th>
					</tr>
				</thead>
				<tbody> 
					<?php 
					if(is_array($data->data)) foreach($data->data as $row){ 
						if($row->Nama_MK == 'TUGAS AKHIR'){
						}else{
							?>
							<tr smt="<?php echo $row->Kode_Prodi; ?>">
								<td><?php echo $row->Kode_Prodi.' '.$row->Nama_Mhs.' ' ?></td>
								<td>
									<?php echo $row->Kode_MK; ?>
								</td>
								<td>
									<?php echo $row->Nama_MK; ?>
								</td>
								<td>
									<?php echo $row->Nilai; ?>
								</td>
								<td>
									<?php echo $row->Tahun; ?>
								</td>
								<td>
									<?php echo $row->Periode_Sem; ?>
								</td>
								<td>
									<?php echo $row->Ambil_Ke; ?>
								</td>
							</tr>
							<?php } 
						}?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
[/section]

[section name="css"]
[/section]

[section name="js"]
<script type="text/javascript">

	function downloadTable()
	{
		$('.mytable input').each(function(){$(this).attr('extype', $(this).attr('type')).attr('type', 'hidden')});
		$('.mytable').table2excel({
			name: '<?php echo PAGE_TITLE; ?>',
			filename: '<?php echo PAGE_TITLE; ?>',
			fileext: '.xls'
		});
		$('.mytable input').each(function(){$(this).attr('type', $(this).attr('extype')).removeAttr('extype')});
	}

	function printTable()
	{
		$('body').addClass('print');
		window.print();
		$('body').removeClass('print');
	}

	$(document).ready(function()
	{


		$('.filter [name="prodi[]"]').change(function()
		{
			$('.mytable').find('tr[smt]').hide();
			$.each($(this).val(), function(key, val)
			{  
				$('.mytable').find('tr[smt="'+val+'"]').show();
			});
		})

			//$('.filter [name="prodi[]"]').change();
			//$('.filter [name="Tahun[]"]').change();
			$('.multi-select').selectpicker('render');
			
			var datatable = $('.mytable').DataTable(
			{
				pageLength : 10,
				lengthChange: false,
				order: [[ 0, "desc" ]],
				responsive: !1,
				dom: "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			}).columns().eq( 0 ).each( function ( colIdx ) 
			{
				
				$( 'input, select', 'thead.filter tr td:nth-child('+colIdx+')' ).on( 'keyup change', function () {
					datatable.column( (colIdx-1) ).search( this.value ).draw();
				} );
				
			} );
		})
	</script>
	[/section]