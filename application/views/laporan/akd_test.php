<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="downloadTable()" data-tooltip data-placement="top" title="Download Excel">
						<i class="icon-cloud-download"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;" onclick="printTable()" data-tooltip data-placement="top" title="Cetak">
						<i class="icon-printer"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-responsive table-bordered table-striped table-hover mytable">
					<thead>
						<tr>
							<th>Kode Prodi</th>
							<th>Nama Prodi</th>
							<th>Jenjang</th>
						</tr>
					</thead>
					<tfoot class="filter" style="display: table-header-group;">
						<tr>
							<td></td>							
							<td></td>
							<td></td>
						</tr>
					</tfoot>
					<tbody>
						<?php foreach ($data as $d) { ?>
						<tr>
							<td><?php echo $d->Kode_Prodi ?></td>
							<td><?php echo $d->Nama_Prodi ?></td>
							<td><?php echo $d->Jenjang ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>
[section name="plugin-css"]
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" />
[/section]

[section name="plugin-js"]
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/custom/plugins/table2excel/jquery.table2excel.js"></script>
[/section]

[section name="css"]
[/section]

[section name="js"]
	<script type="text/javascript">
	
		function downloadTable()
		{
			$('.mytable input').each(function(){$(this).attr('extype', $(this).attr('type')).attr('type', 'hidden')});
			$('.mytable').table2excel({
				name: '<?php echo PAGE_TITLE; ?>',
				filename: '<?php echo PAGE_TITLE; ?>',
				fileext: '.xls'
			}); 
			$('.mytable input').each(function(){$(this).attr('type', $(this).attr('extype')).removeAttr('extype')});
		}
		
		function printTable()
		{
			$('body').addClass('print');
			window.print();
			$('body').removeClass('print');
		}
		$(document).ready(function() {
		    $('.mytable tfoot td').each( function () {
		        var title = $('.mytable thead th').eq( $(this).index() ).text();
		        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+title+'" />' );
		    } );
		    $('.mytable').DataTable().columns().every( function () {
		        var that = this;
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            that.search( this.value ).draw();
		        } );
		    } );
		} );
	</script>
[/section]