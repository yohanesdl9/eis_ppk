[section name="plugin-css"]
	<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
[/section]

[section name="title"]
	<h1><?php echo PAGE_TITLE; ?> <small></small></h1>
[/section]

<div class="row">
	<div class="col-md-12">
		<div class="note note-success">
			<h4 class="block">Selamat Datang di EIS STIKI</h4>
			<p>Disini kami menyajikan laporan-laporan yang datanya berasal dari berbagai sistem yang ada di STIKI, meliputi SIAKAD, SIMKEU, SIMPEG, dll. Dan hak akses untuk tiap-tiap laporan bisa diatur oleh pemilik laporan. </p>
			<br>
			<p>Untuk permintaan penambahan laporan baru atau perbaikan format laporan bisa dilakukan via Helpdesk dengan melampirkan file format laporan yang benar.</p>
		</div>
	</div>
</div>


 <!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat blue">
			<div class="visual">
				<i class="fa fa-file"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo isset($laporan['laporan']) ? $laporan['laporan'] : 0; ?>">0</span>
				</div>
				<div class="desc"> Laporan </div>
			</div>
			<a class="more" href="<?php echo site_url('katalog/laporan'); ?>"> View more
				<i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red">
			<div class="visual">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo isset($laporan['grafik']) ? $laporan['grafik'] : 0; ?>">0</span></div>
				<div class="desc"> Grafik </div>
			</div>
			<a class="more" href="<?php echo site_url('katalog/grafik'); ?>"> View more
				<i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat green-seagreen">
			<div class="visual">
				<i class="fa fa-puzzle-piece"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo isset($laporan['custom']) ? $laporan['custom'] : 0; ?>">0</span>
				</div>
				<div class="desc"> Custom View </div>
			</div>
			<a class="more" href="<?php echo site_url('katalog/custom'); ?>"> View more
				<i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat purple">
			<div class="visual">
				<i class="fa fa-user"></i>
			</div>
			<div class="details">
				<div class="number">
					<span data-counter="counterup" data-value="<?php echo count($user); ?>"></span></div>
				<div class="desc"> User</div>
			</div>
			<a class="more" href="javascript:;"> View more
				<i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->



[section name="plugin-js"]
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
[/section]