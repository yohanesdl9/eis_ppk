<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div id="mychart" style="width: 100%;height	: 500px;"></div>
				
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
<link rel='stylesheet' id='amexport-css' href='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css' type='text/css' media='all'/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]

<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>

<script type='text/javascript' src='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js'></script>
<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

[/section]

[section name="css"]
<style>
	.amcharts-chart-div a {
		display: none !important;
	}
</style>
[/section]

[section name="js"]

<script type="text/javascript">
	var chart = AmCharts.makeChart("mychart", {
		"type": "serial",
		"theme": "light",
		"legend": {
			"useGraphSettings": true
		},
		"dataProvider": <?php echo json_encode($data->data, true); ?>,
		"valueAxes": [{
			"integersOnly": true,
			"axisAlpha": 0,
			"dashLength": 5,
			"gridCount": 10,
			"position": "left",
			"title": "Jumlah"
		}],
		"chartScrollbar": {},
		"startDuration": 0.5,
		"graphs": [
		{
			"balloonText":"Mahasiswa Yang Lolos Seleksi tahun [[category]] sebanyak: [[value]]",
			"bullet":"round",
			"title":"Jml Mahasiswa Lolos Seleksi",
			"valueField":"YES",
			"fillAlphas":0
		}
		,
		{
		 	"balloonText":"Mahasiswa Yang tidak lolos Seleksi tahun [[category]] sebanyak: [[value]]",
		 	"bullet":"round",
		 	"title":"Jml Mahasiswa Tidak Lolos Seleksi",
		 	"valueField":"NO",
		 	"fillAlphas":0
		 }
		],
		"chartCursor": {
			"cursorAlpha": 0,
			"zoomable": false
		},
		"categoryField": "Tahun_Masuk",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"fillAlpha": 0.05,
			"fillColor": "#000000",
			"gridAlpha": 0,
			"position": "bottom"
		},
		"export": {
			"enabled": true
		}
	});

	$('.multi-select').selectpicker('render');

</script>
[/section]