<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">				
				</div>
				
				<div id="mychart" style="width: 100%;height	: 500px;"></div>
				
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel='stylesheet' id='amexport-css' href='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css' type='text/css' media='all'/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]

	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
	
	<script type='text/javascript' src='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js'></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

[/section]

[section name="css"]
	<style>
		.amcharts-chart-div a {
			display: none !important;
		}
		.amcharts-pie-slice {
		  transform: scale(1);
		  transform-origin: 50% 50%;
		  transition-duration: 0.3s;
		  transition: all .3s ease-out;
		  -webkit-transition: all .3s ease-out;
		  -moz-transition: all .3s ease-out;
		  -o-transition: all .3s ease-out;
		  cursor: pointer;
		  box-shadow: 0 0 30px 0 #000;
		}
		.amcharts-pie-slice:hover {
		  transform: scale(1.1);
		  filter: url(#shadow);
		}
	</style>
[/section]

[section name="js"]
	<?php 
		$jadwalkelas = array();
		foreach ($data->data as $row) {
			$jadwalkelas[] = array (
				"nama" => $row->nama,
				"total_jadwal" => $row->total_jadwal
			);
		}		

	?>
	<script>
		var chart = AmCharts.makeChart( "mychart", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": <?php echo json_encode($jadwalkelas);?>,
		  "gridAboveGraphs": true,
		  "startDuration": 1,
		  "graphs": [ {
		    "balloonText": "Banyak Jadwal Kelas dengan Dosen [[category]]: <b>[[value]] jadwal</b>",
		    "fillAlphas": 0.8,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "total_jadwal",
		    "autoColor": true
		  } ],
		  "valueAxes": [{
				"integersOnly": true,
				"axisAlpha": 0,
				"dashLength": 5,
				"gridCount": 10,
				"position": "left",
				"title": "Total Jadwal"
			}],		
		  "chartScrollbar": {},	
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "nama",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "gridAlpha": 0,
		    "tickPosition": "start",
		    "tickLength": 20,
		    "labelRotation": 30
		  },
		  "export": {
		    "enabled": true
		  }

		} );

		<?php echo 'chart.addListener("dataUpdated", function(){ chart.zoomToIndexes('.(count($jadwalkelas)-11).', '.(count($jadwalkelas)-1).'); });'; ?>
		
		$('.multi-select').selectpicker('render');
	</script> 
[/section]