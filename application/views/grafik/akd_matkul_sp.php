<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">				
				</div>
				
				<div id="mychart" style="width: 100%;height	: 500px;"></div>

				<div id="legenddiv" style="width: 100%;height	: 500px;"></div>
				
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel='stylesheet' id='amexport-css' href='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css' type='text/css' media='all'/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]

	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
	
	<script type='text/javascript' src='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js'></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

[/section]

[section name="css"]
	<style>
		.amcharts-chart-div a {
			display: none !important;
		}
		.amcharts-pie-slice {
		  transform: scale(1);
		  transform-origin: 50% 50%;
		  transition-duration: 0.3s;
		  transition: all .3s ease-out;
		  -webkit-transition: all .3s ease-out;
		  -moz-transition: all .3s ease-out;
		  -o-transition: all .3s ease-out;
		  cursor: pointer;
		  box-shadow: 0 0 30px 0 #000;
		}
		.amcharts-pie-slice:hover {
		  transform: scale(1.1);
		  filter: url(#shadow);
		}
	</style>
[/section]

[section name="js"]
	<?php 
		$matkul_sp = array();
		foreach ($data->data as $row) {
			$matkul_sp[] = array (
				"Matkul" => $row->Matkul,
				"Kode_Matkul" => $row->Kode_Matkul,
				"Total" => $row->Total
			);
		}		
	?>
	<script type="text/javascript">
	
		var chart = AmCharts.makeChart("mychart", {
		  "type": "pie",
		  "startDuration": 0,
		   "theme": "light",
		  "addClassNames": true,
		  "legend":{
		   	"position":"bottom",		    
		    "autoMargins":false
		  },
		  "legend": {
		  	"divId": "legenddiv"
		  },
		  "innerRadius": "30%",
		  "defs": {
		    "filter": [{
		      "id": "shadow",
		      "width": "200%",
		      "height": "200%",
		      "feOffset": {
		        "result": "offOut",
		        "in": "SourceAlpha",
		        "dx": 0,
		        "dy": 0
		      },
		      "feGaussianBlur": {
		        "result": "blurOut",
		        "in": "offOut",
		        "stdDeviation": 5
		      },
		      "feBlend": {
		        "in": "SourceGraphic",
		        "in2": "blurOut",
		        "mode": "normal"
		      }
		    }]
		  },
		  "dataProvider": <?php echo json_encode($matkul_sp); ?>,
		  "valueField": "Total",
		  "titleField": "Matkul",
		  "export": {
		    "enabled": true
		  }
		});

		chart.addListener("init", handleInit);

		chart.addListener("rollOverSlice", function(e) {
		  handleRollOver(e);
		});

		function handleInit(){
		  chart.legend.addListener("rollOverItem", handleRollOver);
		}

		function handleRollOver(e){
		  var wedge = e.dataItem.wedge.node;
		  wedge.parentNode.appendChild(wedge);
		}
</script> 
[/section]