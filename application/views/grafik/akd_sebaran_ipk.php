<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa <?php echo PAGE_ICON; ?> font-purple-plum"></i>
					<span class="caption-subject bold font-purple-plum uppercase"> <?php echo PAGE_TITLE; ?> </span>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID); ?>" data-tooltip data-placement="top" title="Kembali">
						<i class="icon-action-undo"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default" href="<?php echo site_url(PAGE_ID.'/setting/'.PAGE_CODE)?>" data-toggle="modal" data-target="#main-modal-md" data-tooltip data-placement="top" title="Setting">
						<i class="icon-wrench"></i>
					</a>
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-tooltip data-placement="top" title="Fullscreen"> </a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row filter margin-b-15 hide-print hide">
					<?php echo json_encode($data->header); ?>
					<div class="form-group">
						<label class="col-md-2 control-label">Sebaran IPK</label>
						<div class="col-md-4">
							<select class="form-control multi-select" name="prodi[]" multiple>
								<?php 
									$inclusion = array('Seb-1', 'Seb-2', 'Seb-3');
									foreach($data->header as $p)
									{
										$selected = in_array($p->id_sebaran, $inclusion) ? 'selected="selected"' : '';
										echo '<option value="'.$p->id_sebaran.'" '.$selected.'>'.$p->id_sebaran.'</option>';
									}
								?>
							</select>
						</div>
						<label class="col-md-2 control-label">Periode Smt</label>
						<div class="col-md-4">
							<select class="form-control multi-select" name="periode[]" multiple>
								<?php 
									$inclusion = array('Ganjil','Genap');
									foreach($periode as $p) 
									{
										$selected = in_array($p, $inclusion) ? 'selected="selected"' : '';
										echo '<option value="'.$p.'" '.$selected.'>'.$p.'</option>';
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<form class="form-horizontal" action="" method="post">
					<div class="form-group">
						<label class="col-md-1 control-label">Pilih Prodi</label>
						<div class="col-md-4">
							<select class="form-control" name="select_prodi" onchange="submit()">
								<option value="Semua Prodi" <?php echo $selected_prodi == 'Semua Prodi' ? 'selected="selected"' : '' ?>>Semua Prodi</option>
								<?php 
									foreach($prodi as $p){
										$selected = $selected_prodi == $p->Kode_Prodi ? 'selected="selected"' : '';
										echo '<option value="'.$p->Kode_Prodi.'" '.$selected.'>'.$p->Kode_Prodi.'</option>';
									}
								?>
							</select>
						</div>
					</div>
				</form>
				<div id="mychart" style="width: 100%;height	: 500px;"></div>
				
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
</div>

[section name="plugin-css"]
	<link rel='stylesheet' id='amexport-css' href='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.css' type='text/css' media='all'/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>"/>
[/section]

[section name="plugin-js"]

	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
	
	<script type='text/javascript' src='<?php echo base_url()?>assets/global/plugins/amcharts/amcharts/plugins/export/export.min.js'></script>
	<script type="text/javascript" src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

[/section]

[section name="css"]
	<style>
		.amcharts-chart-div a {
			display: none !important;
		}
	</style>
[/section]

[section name="js"]
	<?php
		
		$prodi = array();
		$data_new = array();
		$header_grafik = array();
		/* header grafik */
		for ($i = 0; $i < count($data->header); $i++)
		{
			$prodi[$data->header[$i]['id_sebaran']] = $data->header[$i];
			$key = str_replace('-', '_', $data->header[$i]['id_sebaran']);	
			$header_grafik[] = array(
				"balloonText" => "IPK " . $data->header[$i]['rentang'] . " : [[value]]",
				"bullet" => "round",
				"title" => "IPK " . $data->header[$i]['rentang'],
				"valueField" => $key,
				"fillAlphas" => 0
			);
		}
		/* data */
		foreach($data->data as $row) if( in_array($row->Periode_Sem, array('Ganjil', 'Genap')))
		{
			$row_new['Periode'] = $row->Tahun.' '.$row->Periode_Sem;
			foreach($prodi as $key => $val)
			{
				$key_new = str_replace('-', '_', $key);
				$key_new_2 = $key_new;
				$row_new[$key_new] = number_format($row->$key_new_2);
			}
			$data_new[] = $row_new;
		}
	
	?>
	<script type="text/javascript">
		var chart = AmCharts.makeChart("mychart", {
			"type": "serial",
			"theme": "light",
			"legend": {
				"useGraphSettings": true
			},
			"dataProvider": <?php echo json_encode($data_new, true); /* menyimpan data dalam bentuk JSON untuk dieksekusi ke dalam bentuk grafik */ ?>,
			"valueAxes": [{
				"integersOnly": true,
				"axisAlpha": 0,
				"dashLength": 5,
				"gridCount": 10,
				"position": "left",
				"title": "Jumlah Mahasiswa"
			}],
			"chartScrollbar": {},
			"startDuration": 0.5,
			"graphs": <?php echo json_encode($header_grafik); /* untuk membuat garis */ ?>,
			"chartCursor": {
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "Periode",
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha": 0,
				"fillAlpha": 0.05,
				"fillColor": "#000000",
				"gridAlpha": 0,
				"position": "bottom"
			},
			"export": {
				"enabled": true
			 }
		});
		
		<?php echo 'chart.addListener("dataUpdated", function(){ chart.zoomToIndexes('.(count($data_new)-11).', '.(count($data_new)-1).'); });'; ?>
		
		$('.multi-select').selectpicker('render');

	</script>
	</script>
[/section]