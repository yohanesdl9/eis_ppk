<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function generateBreadcrumb($activePage)
{
    $model = get_instance();
    $model->load->model('sys_model');
    
    $breadcrumb = '<ul class="page-breadcrumb breadcrumb">';
	$breadcrumb.='<li><a href="'.site_url().'">Home</a><i class="fa fa-circle"></i></li>';
	
	$lastPage = $model->sys_model->getBreadCrumb($activePage);
    $url = site_url($lastPage['URL']);
    if($lastPage['Induk_Menu']=='' || $lastPage['Induk_Menu']=='0')
	{
        $breadcrumb.='<li><a href="'.$url.'">'.$lastPage['Nama_Menu'].'</a><i class="fa fa-circle"></i></li>';
    }else{
        $breadcrumb.= generateParent($lastPage['Induk_Menu']);
        $breadcrumb.= '<li><a href="'.$url.'">'.$lastPage['Nama_Menu'].'</a><i class="fa fa-circle"></i></li>';
    }
	
	if(defined('PAGE_CODE'))
	{
		$breadcrumb.= '<li><a href="#">'.PAGE_CODE.'</a><i class="fa fa-circle"></i></li>';
	}
    
    $breadcrumb.='</ul>';
    
    return $breadcrumb;
}

function generateParent($id)
{
    $model = get_instance();
    $model->load->model('sys_model');
    
    $breadcrumb='';    
	$lastPage = $model->sys_model->getBreadCrumbByMenuId($id);
    $url=site_url($lastPage['URL']);
    if($lastPage['Induk_Menu']=='' || $lastPage['Induk_Menu']=='0'){
        $breadcrumb.='<li>
					<a href="'.$url.'">'.$lastPage['Nama_Menu'].'</a><i class="fa fa-circle"></i>
                    
				</li>';
    }else{
        $breadcrumb.=generateParent($lastPage['Induk_Menu']);
        $breadcrumb.='<li>
					<a href="'.$url.'">'.$lastPage['Nama_Menu'].'</a><i class="fa fa-circle"></i>
                    
				</li>';
    }
    
    return $breadcrumb;
}

function generateMenu(){

    //$model = get_instance();    
    $ci =& get_instance();
    $ci->load->model('sys_model');
    
    $site_title = $ci->lang->line('site_title');
    
    $strHTML='';
    $strHTMLMenu='<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">';
    
	$dataInduk = $ci->sys_model->get_induk();    
   
    foreach($dataInduk as $indukList){
        if($indukList['URL']=='' || $indukList['URL']=='#'){
            $baseurl='javascript:;';
        }else{
            $baseurl = site_url($indukList['URL']);
        }
            
        if (count($indukList['child'])==0){
            //untuk yg tidak ada child
            $strHTMLMenu .= '<li class="nav-item">
                                <a href="'.$baseurl.'" id="'.$indukList['Menu_id'].'" class="nav-link ">
                                    <i class="fa '.$indukList['Icon'].'"></i> 
                                    <span class="title">'.$indukList['Nama_Menu'].'</span>
                                </a>
                            </li>';
        }else{
            $strHTMLMenu .= '<li class="nav-item">
                                <a href="javascript:;" id="'.$indukList['Menu_id'].'" class="nav-link ">
                                    <i class="fa '.$indukList['Icon'].'"></i> 
                                    <span class="title">'.$indukList['Nama_Menu'].'</span>
									<span class="arrow "></span>
                                </a>';
//            
            $strHTMLMenu .= '<ul class="sub-menu">';            
            $strHTMLMenu .= generateSubMenu($indukList['Menu_id']);
            $strHTMLMenu .= '</ul></li>';
            
        }
        
        
        
    }
    $strHTMLMenu.='</ul>';
//    //return $userInfo.$userName.$userNrp.$userRole.$strHTMLMenu.$strHTML;
    //return $strHTMLMenu.$strHTML.$userStr;
    return $strHTMLMenu;
}

function generateSubMenu($induk){
    $model = get_instance();
    $model->load->model('sys_model');
    $dataChild = $model->sys_model->get_child($induk);
        
    $str='';
    foreach($dataChild as $listChild){
        if($listChild['URL']=='' || $listChild['URL']=='#'){
            $baseurl='javascript:;';
        }else{
            $baseurl = site_url($listChild['URL']);
        }
        if(count($listChild['child'])!= 0){               
            $str .= '<li>
                        <a href="javascript:;" id="'.$listChild['Menu_id'].'">
                            <i class="fa '.$listChild['Icon'].'"></i> 
                            '.$listChild['Nama_Menu'].'
							<span class="arrow"></span>
                        </a>';
            $str .= '<ul class="sub-menu">';
            $str .= generateSubMenu($listChild['Menu_id']);
            $str .= "</ul></li>";
        }else{
            $str .= '<li>
                        <a href="'.$baseurl.'" id="'.$listChild['Menu_id'].'">
                            <i class="fa '.$listChild['Icon'].'"></i> 
                            '.$listChild['Nama_Menu'].'
                        </a>
                    </li>';
        }
    }
    return $str;
}
