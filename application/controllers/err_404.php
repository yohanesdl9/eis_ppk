<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class err_404 extends admin_controller {


	public function index()
	{
		// if ajax
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			$this->load->view('template/404', array('is_ajax' => true));
		}
		else
		{
			$this->load->template('template/404', array('is_ajax' => false));
		}
	}
	
}
