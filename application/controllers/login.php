<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{
        $is_login = $this->session->userdata('logged_in');
		
		if ($is_login) redirect(site_url('dashboard'));
		
		$wtoken = $this->session->userdata('wtoken');
		/* if(!$wtoken)
		{
			$ws = $this->sys_model->getConfigItem('ws_portal');
            $m = $this->sys_model->getConfigItem('mikrotik_login');
            $s = $this->sys_model->getConfigItem('mikrotik_sender');
			redirect($m.'/'.$s.'#'.site_url('login/receiver'));
		} */
		
		if ($this->input->server('REQUEST_METHOD') == 'POST') 
		{
			$this->sys_model->_clearExpiratedSess();
            $u = trim($this->input->post('username'));
            $p = trim($this->input->post('password'));
            $u = preg_replace('/[^A-Za-z0-9\@\.]/','',$u);
			
			if($u == '' || $p == '') redirect(site_url());
			
            $t=$this->getTracking($this->input->post());
            $res = $this->sys_model->doCekLogin($u,$p,$t);
			
			if ($res)
				redirect(site_url('dashboard'));
			else
				redirect(base_url());
            
			return true;
		}
        if($this->session->userdata('isBanned'))
            $this->load->view('banned');
        else
			$this->load->view('login',$d);   

	}
	
    public function auth()
	{
        $id=$this->input->post('id');
        $data['isOk']=$this->sys_model->isBrowserBanned($id);
        $this->session->set_userdata('isBanned',$data['isOk']);
        echo json_encode($data);
    }
    
	public function getTracking($p)
	{
        if(isset($p['browser']) && isset($p['fid']) && isset($p['appVersion'])){
            $res=array();
            $browser=json_decode($p['browser']);
            foreach($browser as $key=>$b){
                if($key!='webkit' && $b===true) $res['browser']=($key.'/'.$browser->version);
            }
            $res['fid']=$p['fid'];
            
            $app=explode(' ',$p['appVersion']);
            $os=$app['1'].' '.$app['2'].' '.$app['3'];
            $os=substr($os,1,strlen($os)-2);
            $res['os']=$os;
            $osName=$this->sys_model->getOsMarketName($os);
            if(!empty($osName)) $res['os'].=(' ('.$osName.')');
            
            $token=base64_decode(base64_decode($p['wtoken']));
            $t=json_decode($token);
            
            if(empty($t->i))
            $res['ip']=$this->input->ip_address();
            else $res['ip']=$t->i;
            
            $res['mac']=$t->m;
            $res['NUser']=$t->u;
            
        }else $res=false;
        return $res;
    }
	
	public function logout()
	{
		session_destroy();
		if(!$this->session->userdata('logged_in'))
		{
			redirect(base_url());
		}
		else
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}

	public function receiver($param = false)
	{
		if (!$param || $_SERVER['HTTP_REFERER'] != 'http://login.stiki.ac.id/sender.html')
		{
			show_404();
			die;
		}
		$this->session->set_userdata('wtoken', $param);
		redirect(base_url());
	}

	
}