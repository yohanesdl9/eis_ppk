<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test extends Laporan_Controller {
	public function index(){
		$this->load->model('mod_akademik_baru');
		$prodi = $this->input->post('prodi') ? $this->input->post('prodi') : 'TI-S1';
		$pack = array(
			'list_prodi' => $this->db->get('tb_akd_rf_prodi')->result(),
			'selected_prodi' => $prodi,			
			'data' => $this->mod_akademik_baru->getMahasiswaAktifPerTahun($prodi)
		);
		$this->load->template('laporan/akd_cobacoba', $pack);
	}
}
