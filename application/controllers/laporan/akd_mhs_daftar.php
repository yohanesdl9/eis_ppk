<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_daftar extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMahasiswaDaftar()
		);
		$this->load->template('laporan/akd_mhs_daftar', $pack);
	}
	public function form($tahun)
	{
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->akd_form_mhs_daftar($tahun)
			);
		$this->load->template('laporan/akd_form_mhs_daftar', $pack);
		
	}
}