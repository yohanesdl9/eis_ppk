<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kehadiran_dosen extends Laporan_Controller {
	public function index(){
		$this->load->model('mod_akademik');
		$smt = $this->input->post('periode_smt') ? $this->input->post('periode_smt') : '2013 Genap';
		$prodi = $this->input->post('prodi') ? $this->input->post('prodi') : 'TI-S1';
		$semester = explode(' ', $smt);
		$pack = array(
			'periode_smt' => $smt,
			'prodi' => $prodi,
			'periode' => $this->db->query("SELECT DISTINCT CONCAT_WS(' ', N.Tahun, N.Periode_Sem) as periode
				FROM tb_skm_tr_presensi AS N ORDER BY N.Tahun, N.Periode_Sem ASC")->result(),
			'data' => $this->mod_akademik->getKehadiranDosen($semester[0], $semester[1], $prodi)
		);
		$this->load->template('laporan/akd_kehadiran_dosen', $pack);
	}
	
	public function form($tahun, $semester, $prodi, $kodemk, $kelas, $nip, $mhs){
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->getDetailKehadiranMhs($tahun, $semester, $prodi, $kodemk, $kelas, $nip)->result(),
			'matkul' => $this->db->where('Kode_MK', $kodemk)->get('tb_akd_rf_mata_kuliah')->row(),
			'kelas' => $kelas,
			'dosen' => $this->db->where('nip', $nip)->get('tb_peg_rf_pegawai')->row(),
			'periode_smt' => $tahun . ' ' . $semester, 
			'total_mhs' => $mhs
		);
		$this->load->template('laporan/akd_form_kehadiran_dosen', $pack);
	}
}