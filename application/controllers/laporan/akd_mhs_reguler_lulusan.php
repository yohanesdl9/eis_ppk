<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_reguler_lulusan extends Laporan_Controller {

	public function index()
	{	
		$this->load->model('mod_misc');		
		$this->load->model('mod_akademik');
		$pack = array(		
			'data' => $this->mod_akademik->getAllMhsRegulerLulusan()
		);	
		$this->load->template('laporan/akd_mhs_reguler_lulusan', $pack);
	}
	
}
