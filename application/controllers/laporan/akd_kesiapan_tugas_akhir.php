<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kesiapan_tugas_akhir extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$this->load->model('mod_misc');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getakd_kesiapan_tugas_akhir(),
			);
		$this->load->template('laporan/akd_kesiapan_tugas_akhir', $pack);
	}
	public function form($id){
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->getform_akd_kesiapan_tugas_akhir($id)
			);
		//view buat baru dengan nama akd_form_kelas_sp
		$this->load->template('laporan/akd_form_kesiapan_TA', $pack);
	}

	
	
	
}

