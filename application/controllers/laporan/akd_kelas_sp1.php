<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kelas_sp1 extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getALLSPdata(),
			);
		$this->load->template('laporan/akd_kelas_sp1', $pack);
	}

	
	
	
}

