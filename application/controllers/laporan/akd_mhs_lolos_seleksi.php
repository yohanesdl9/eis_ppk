<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_lolos_seleksi extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMhsLolosSeleksi()
		);
		$this->load->template('laporan/akd_mhs_lolos_seleksi', $pack);
	}
	public function form($Tahun_Masuk)
	{
		// $this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->akd_form_mhs_lolos_seleksi($Tahun_Masuk),
			);
		$this->load->template('laporan/akd_form_mhs_lolos_seleksi', $pack);
		
	}
}