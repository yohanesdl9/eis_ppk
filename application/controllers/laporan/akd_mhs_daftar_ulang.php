<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_daftar_ulang extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMhsDaftarUlang()
		);
		$this->load->template('laporan/akd_mhs_daftar_ulang', $pack);
	}
	public function form($tahun)
	{
		// $this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->akd_form_mhs_daftar_ulang($tahun),
			);
		$this->load->template('laporan/akd_form_mhs_daftar_ulang', $pack);
		
	}
}