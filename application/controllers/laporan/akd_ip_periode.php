<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class akd_ip_periode extends Laporan_Controller {
	public function index()
	{
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getRataIPPerPeriode(),
		);
		$this->load->template('laporan/akd_ip_periode', $pack);
	}
}