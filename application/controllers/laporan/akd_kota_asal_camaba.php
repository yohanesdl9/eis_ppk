<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kota_asal_camaba extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(	
			'data' => $this->mod_akademik->getCamabaProp()
		);
		$this->load->template('laporan/akd_prop_asal_camaba', $pack);
	}

	public function form($Prop)
	{
		$this->load->model('mod_akademik');
		$pack = array(	
			'data' => $this->mod_akademik->getCamabaKota(str_replace('%20', ' ', $Prop))
		);
		$this->load->template('laporan/akd_kota_asal_camaba', $pack);
	}

}