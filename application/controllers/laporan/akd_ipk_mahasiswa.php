<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_ipk_mahasiswa extends Laporan_Controller {
	public function index(){
		$this->load->model('mod_akademik');
		$smt = $this->input->post('periode_smt') ? $this->input->post('periode_smt') : '2013 Genap';
		$prodi = $this->input->post('prodi') ? $this->input->post('prodi') : 'TI-S1';
		$semester = explode(' ', $smt);
		$pack = array(
			'periode_smt' => $smt,
			'prodi' => $prodi,
			'periode' => $this->db->query("SELECT DISTINCT CONCAT_WS(' ', N.Tahun, N.Periode_Sem) as periode
				FROM tb_akd_tr_statistik_nilai AS N ORDER BY N.Tahun, N.Periode_Sem ASC")->result(),
			'data' => $this->mod_akademik->getIPKMahasiswa($semester[0], $semester[1], $prodi)
		);
		$this->load->template('laporan/akd_ipk_mahasiswa', $pack);
	}
}
