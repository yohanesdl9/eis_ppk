<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_dosen_wali extends Laporan_Controller {
	public function index(){
		$this->load->model('mod_akademik');
		$smt = $this->input->post('periode_smt') ? $this->input->post('periode_smt') : '2013 Genap';
		$prodi = $this->input->post('prodi') ? $this->input->post('prodi') : 'TI-S1';
		$semester = explode(' ', $smt);
		$pack = array(
			'periode_smt' => $smt,
			'prodi' => $prodi,
			'periode' => $this->db->query("SELECT DISTINCT CONCAT_WS(' ', N.Tahun, N.Periode_Sem) as periode
				FROM tb_akd_tr_status_mahasiswa AS N ORDER BY N.Tahun, N.Periode_Sem ASC")->result(),
			'data' => $this->mod_akademik->getMhsAktifperDosenWali($semester[0], $semester[1], $prodi)
		);
		$this->load->template('laporan/akd_mhs_dosen_wali', $pack);
	}
	
	public function form($tahun, $semester, $prodi, $nip){
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->getFormMhsPerDosenWali($tahun, $semester, $prodi, $nip),
			'dosen' => $this->db->where('nip', $nip)->get('tb_peg_rf_pegawai')->row(),
			'periode_smt' => $tahun . ' ' . $semester
		);
		$this->load->template('laporan/akd_form_mhs_dosen_wali', $pack);
	}
}
