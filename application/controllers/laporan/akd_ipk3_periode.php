<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_ipk3_periode extends Laporan_Controller {

	public function index()
	{
		$min = $this->input->post('ipk_min') && $this->checkIPRange($this->input->post('ipk_min')) ? $this->input->post('ipk_min') : 0;
		$max = $this->input->post('ipk_max') && $this->checkIPRange($this->input->post('ipk_max')) ? $this->input->post('ipk_max') : 4;
		if ($min > $max){
			$temp = $min;
			$min = $max;
			$max = $temp;
		}
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getIPK3PerPeriode($min, $max),
			'min' => $min,
			'max' => $max
		);
		$this->load->template('laporan/akd_ipk3_periode', $pack);
	}
	
	public function checkIPRange($ip){
		if ($ip < 0) return false;
		if ($ip > 4) return false;
		return true;	
	}
}
