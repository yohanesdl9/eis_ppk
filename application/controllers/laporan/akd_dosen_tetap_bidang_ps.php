<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_dosen_tetap_bidang_ps extends Laporan_Controller {

	public function index()
	{	
		$this->load->model('mod_misc');		
		$this->load->model('mod_akademik');
		$pack = array(		
			'data' => $this->mod_akademik->getDosenTetapKeahlianPS()
		);	
		$this->load->template('laporan/akd_dosen_tetap_bidang_ps', $pack);
	}
	
}
