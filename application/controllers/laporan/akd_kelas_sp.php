<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kelas_sp extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getALLSPdata(),
			);
		$this->load->template('laporan/akd_kelas_sp', $pack);
	}
	public function form($id){
	$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->akd_form_kelas_sp($id),
			);
		//view buat baru dengan nama akd_form_kelas_sp
		$this->load->template('laporan/akd_form_kelas_sp', $pack);
	}


	
	
	
}

