<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_kota extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(		
			'data' => $this->mod_akademik->getMhsKota()
		);
		$this->load->template('laporan/akd_mhs_kota', $pack);
	}
}
