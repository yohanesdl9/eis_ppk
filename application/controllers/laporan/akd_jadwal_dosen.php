<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class akd_jadwal_dosen extends Laporan_Controller {
	public function index()
	{
		$this->load->model('mod_akademik');
		$smt = $this->input->post('periode_smt') ? $this->input->post('periode_smt') : '2014 Genap';
		$semester = explode(' ', $smt);
		$pack = array(
			'periode_smt' => $smt,
			'periode' => $this->mod_akademik->getPeriodeJadwal(),
			'data' => $this->mod_akademik->getJadwalKelasByPeriode($semester[0], $semester[1])
		);
		$this->load->template('laporan/akd_jadwal_dosen', $pack);
	}
	
	public function form($nama, $tahun, $periode)
	{
		$this->load->model('mod_akademik');
		$pack = array(
			'data' => $this->mod_akademik->akd_form_jadwal_dosen($nama, $tahun, $periode),
			'dosen' => $this->db->where('nip', $nama)->get('tb_peg_rf_pegawai')->row(),
			'periode_smt' => $tahun . ' ' . $periode
		);
		$this->load->template('laporan/akd_form_jadwal_dosen', $pack);
	}
}