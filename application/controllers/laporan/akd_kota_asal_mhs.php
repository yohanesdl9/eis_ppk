<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_kota_asal_mhs extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akd_kota_asal');
		$pack = array(		
			'data' => $this->mod_akd_kota_asal->getMhsKota()
		);
		$this->load->template('laporan/akd_kota_asal_prop', $pack);
	}

}
