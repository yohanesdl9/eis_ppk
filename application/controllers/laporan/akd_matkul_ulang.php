<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_matkul_ulang extends Laporan_Controller {

	public function index()
	{		
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMatkulUlang()
		);
		$this->load->template('laporan/akd_matkul_ulang.php', $pack);
	}
	
	
	
	
}
