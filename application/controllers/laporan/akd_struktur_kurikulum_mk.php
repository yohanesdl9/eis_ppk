<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_struktur_kurikulum_mk extends Laporan_Controller {

	public function index()
	{	
		$this->load->model('mod_misc');		
		$this->load->model('mod_akademik');
		$pack = array(	
			'data' => $this->mod_akademik->getStrukturKurikulumMK()
		);	
		$this->load->template('laporan/akd_struktur_kurikulum_mk', $pack);
	}
	
}
