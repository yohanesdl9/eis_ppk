<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class akd_mhs_angkatan_tahun extends Laporan_Controller {
	public function index(){	
		$this->load->model('mod_misc');		
		$this->load->model('mod_akademik');
		$prodi = $this->input->post('prodi') ? $this->input->post('prodi') : 'TI-S1';
		$status = $this->input->post('status') ? $this->input->post('status') : 'ALL';
		$pack = array(
			'list_prodi' => $this->db->get('tb_akd_rf_prodi')->result(),
			'selected_prodi' => $prodi,		
			'list_status' => $this->db->get('tb_akd_rf_status_akademis')->result(),
			'selected_status' => $status,
			'data' => $this->mod_akademik->getJumlahMhsAngkatan($prodi, $status)
		);	
		$this->load->template('laporan/akd_mhs_angkatan_tahun', $pack);
	}
	
}
