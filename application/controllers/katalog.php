<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class katalog extends admin_controller {

	public function index($jenis)
	{
		$pack = array(
			'jenis' => $jenis,
		);
		$this->load->template('katalog', $pack);
	}
	
	public function getDatatable($jenis)
	{
		$this->load->library('datatable');
		$column = array('Kode_Laporan', 'Deskripsi', 'Jenis');
		$query = "SELECT
			l.*, 
			COALESCE(isView, 'YES') as isView,
			COALESCE(isNew, 'YES') as isNew
		FROM
			".DB_EIS."tb_eis_rf_laporan l
		LEFT JOIN
			".DB_EIS."tb_eis_tr_akses_laporan a ON l.Kode_Laporan=a.Kode_Laporan AND a.Username = '".USERNAME."'
		WHERE
			l.Jenis = '$jenis'
		";
			
		$data = $this->datatable->render($column, $query, 'Kode_Laporan', true, true);
		
		$data_new = array();
		foreach($data->data as $i => $row)
		{
			$is_new = $row->isNew == 'YES' ? ' &nbsp;<span class="badge badge-danger"> new </span>' : '';
			$view_url = site_url(PAGE_ID.'/view/'.$row->Kode_Laporan);
			
			if($row->isView != 'YES')
			{
				$data_new[] = array(
					$row->Kode_Laporan,
					$row->Deskripsi . $is_new,
					'<a href="'.site_url(PAGE_ID.'/setting/'.$row->Kode_Laporan).'" class="btn btn-xs btn-default" data-toggle="modal" data-target="#main-modal-md"><i class="fa fa-wrench"></i></a>'
				);
			}
			else
			{
				$data_new[] = array(
					'<a href="'.$view_url.'">'.$row->Kode_Laporan.'</a>',
					'<a href="'.$view_url.'">'.$row->Deskripsi.'</a>' . $is_new,
					'<a href="'.$view_url.'" class="btn btn-xs btn-default"><i class="fa fa-file"></i></a>
					<a href="'.site_url(PAGE_ID.'/setting/'.$row->Kode_Laporan).'" class="btn btn-xs btn-default" data-toggle="modal" data-target="#main-modal-md"><i class="fa fa-wrench"></i></a>'
				);
			}
		}
		$data->data = $data_new;
		echo json_encode($data);
	}
	
	public function setting($jenis, $kode)
	{
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST') 
		{
			if(!$this->mod_laporan->isImPermitted('view', $kode)) show_404();
			if($this->mod_laporan->saveAkses($kode, $_POST['data'])) echo 'ok';
			return false;
		}
		$pack = array(
			'data' => $this->mod_laporan->getSingleData($kode),
			'user' => $this->mod_laporan->getAllPermittedUser($kode),
			'is_admin' => $this->mod_laporan->isImPermitted('admin', $kode),
			'jenis' => $jenis,
			'kode' => $kode,
		);
		$this->load->view('katalog-setting', $pack);
	}
}
