<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends Laporan_Controller {

	public function index()
	{
		$pack = array(
			'cols' => json_encode(array('Angkatan')),
			'rows' => json_encode(array('Prodi'))
		);
		$this->load->template('template/custom_view', $pack);
	}
	
	public function getDataJSON()
	{
		$this->load->model('mod_akademik');
		$data = $this->mod_akademik->getAllData();
		echo json_encode($data);
	}
	
	
	
}
