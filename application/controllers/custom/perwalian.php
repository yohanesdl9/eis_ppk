<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perwalian extends Laporan_Controller {

	public function index()
	{
		$pack = array(
			'cols' => json_encode(array('Tahun', 'Semester')),
			'exclusions' => json_encode(
				array(
					'Tahun' => array('1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009'),
					'Semester' => array('1_prf','2_prf','3_prf','Pendek_Ganjil','Pendek_Genap' ),
				)
			)
		);
		$this->load->template('template/custom_view', $pack);
	}
	
	public function getDataJSON()
	{
		$this->load->model('mod_perwalian');
		$data = $this->mod_perwalian->getAllData();
		echo json_encode($data);
	}
	
	
	
}
