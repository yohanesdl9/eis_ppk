<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_sebaran_ipk extends Laporan_Controller {

	public function index(){
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$selected_prodi = $this->input->post('select_prodi') ? $this->input->post('select_prodi') : 'Semua Prodi';
		$pack = array(
			'selected_prodi' => $selected_prodi,
			'prodi' => $this->db->get('tb_akd_rf_prodi')->result(),
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getSebaranIPK($selected_prodi),
		);
		$this->load->template('grafik/akd_sebaran_ipk', $pack);
	}

}
