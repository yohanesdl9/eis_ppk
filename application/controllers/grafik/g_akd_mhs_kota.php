<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_mhs_kota extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMhsKota()
		);
		$this->load->template('grafik/akd_mhs_kota', $pack);
	}
	
	
	
	
}
