<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_mhs_lolos_seleksi extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMhsLolosSeleksi()
		);
		$this->load->template('grafik/akd_mhs_lolos_seleksi', $pack);
	}	
}