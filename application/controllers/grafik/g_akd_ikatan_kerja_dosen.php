<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_ikatan_kerja_dosen extends Laporan_Controller {

	public function index()
	{			
		$this->load->model('mod_akademik');
		$pack = array(		
			'data' => $this->mod_akademik->getIkatanKerjaDosen()
		);
		
		$this->load->template('grafik/akd_ikatan_kerja_dosen', $pack);
	}
	
 
}

		