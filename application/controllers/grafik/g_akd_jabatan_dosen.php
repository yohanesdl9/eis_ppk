<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_jabatan_dosen extends Laporan_Controller {

	public function index()
	{		
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getJabatan()
		);
		$this->load->template('grafik/akd_jabatan_dosen', $pack);
	}
	
	
	
	
}
