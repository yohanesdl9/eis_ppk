<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_jadwal_dosen extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getJadwalKelas()
		);
		$this->load->template('grafik/akd_jadwal_dosen', $pack);
	}
	
	
	
	
}
