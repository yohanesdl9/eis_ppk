<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_mhs_daftar extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMhsDaftar()

		);
		
		$this->load->template('grafik/akd_mhs_daftar', $pack);
	}	
}