<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_matkul_ulang extends Laporan_Controller {

	public function index()
	{		
		$this->load->model('mod_akademik');
		$pack = array(			
			'data' => $this->mod_akademik->getMatkulUlang()
		);
		$this->load->template('grafik/akd_matkul_ulang.php', $pack);
	}
	
	
	
	
}
