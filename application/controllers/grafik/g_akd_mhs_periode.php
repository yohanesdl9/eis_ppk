<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class g_akd_mhs_periode extends Laporan_Controller {

	public function index()
	{
		$this->load->model('mod_misc');
		$this->load->model('mod_akademik');
		$pack = array(
			'periode' => $this->mod_misc->getAllPeriodeSmt(),
			'data' => $this->mod_akademik->getMhsPeriode(),
		);
		$this->load->template('grafik/akd_mhs_periode', $pack);
	}
	
	
	
	
}
