<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends admin_controller {

	public function index()
	{
		$pack = array(
			'laporan' => $this->mod_laporan->getJmlLaporanPerJenis(),
			'user' => $this->mod_laporan->getAllPermittedUser(),
		);
	
		$this->load->template('dashboard', $pack);
	}
	
	function change_pass()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') 
		{
			if ( $this->sys_model->changePassword($_POST['old'],$_POST['new'],$_POST['confirm']) ) echo 'ok';
			die;
		}
		$this->load->template('change_pass');
	
	}
	
	
}
