<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_akd_kota_asal extends CI_MODEL
{
	function getMhsKota(){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Total_Kota END) Total_".$initial;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT Kota, Provinsi, $sql_header, SUM(Total_Kota) AS Total
		FROM (SELECT m.Kota_asl as Kota, p.Nama_Prop as Provinsi, m.Kode_Prodi, COUNT(*) AS Total_Kota 
		FROM tb_akd_rf_mahasiswa AS m, tb_glb_rf_kota AS k, tb_akd_rf_propinsi AS p
		WHERE m.Kota_asl = k.Nama_Kota AND p.Kode_Prop = k.Kode_Prop
		GROUP BY m.Kota_asl, m.Kode_Prodi) as main
		GROUP BY Kota";
		$data = $this->db->query($query)->result();
		
		return (object)array(
			'header' => $header,			
			'data' => $data
		);
	}
}