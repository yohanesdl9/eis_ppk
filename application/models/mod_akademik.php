<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mod_akademik extends CI_MODEL
{
	function akd_form_mhs_daftar_ulang($tahun){
		$header = $this->db->get(DB_AKD.'tb_pmb_tr_camaba_reg')->result();
		if (!$header) return false;
		// $sql_header = array();
		// foreach($header as $row)
		// {
		// 	$initial = str_replace('-','_', $row->Tahun_Masuk,$row->Nama_Mhs);
		// 	$sql_header[] = " MAX(CASE WHEN Tahun_Masuk  = '".$row->Tahun_Masuk."' THEN Jml_Nilai END) Jml_".$initial;
		// }
		// $sql_header = implode(',',$sql_header);

		$sql = "
		SELECT
		b.Tahun_Penerimaan,
		b.nama,
		b.telp,
		b.email
		FROM
		tb_pmb_tr_camaba_reg AS b
		WHERE
		b.Tahun_Penerimaan = '".$tahun."'";

		$data = $this->db->query($sql)->result();
		return (object)array(
			// 'header' => $header,
			'data' => $data
			);
	}
	function akd_form_mhs_lolos_seleksi($Tahun_Masuk){
		$header = $this->db->get(DB_AKD.'tb_pmb_tr_camaba')->result();
		if (!$header) return false;
		// $sql_header = array();
		// foreach($header as $row)
		// {
		// 	$initial = str_replace('-','_', $row->Tahun_Masuk,$row->Nama_Mhs);
		// 	$sql_header[] = " MAX(CASE WHEN Tahun_Masuk  = '".$row->Tahun_Masuk."' THEN Jml_Nilai END) Jml_".$initial;
		// }
		// $sql_header = implode(',',$sql_header);

		$sql = "SELECT
		a.NRP,
		a.Nama_Mhs,
		a.Tahun_Masuk,
		a.IsDiterima
		FROM
		tb_pmb_tr_camaba AS a
		WHERE
		a.Tahun_Masuk = '".$Tahun_Masuk."' AND a.IsDiterima = 'YES'
		UNION
		SELECT
		a.NRP,
		a.Nama_Mhs,
		a.Tahun_Masuk,
		a.IsDiterima
		FROM
		tb_pmb_tr_camaba AS a
		WHERE
		a.Tahun_Masuk = '".$Tahun_Masuk."' AND a.IsDiterima = 'NO'";
		echo $query;
		$data = $this->db->query($sql)->result();
		return (object)array(
			// 'header' => $header,
			'data' => $data
			);
	}
	function getMhsLolosSeleksi()
	{

		$query = "SELECT
		t.Tahun_Masuk,
		COUNT(t.IsDiterima) AS YES,
		l.`NO`,
		IF(`NO` IS NULL, COUNT(t.IsDiterima), COUNT(t.IsDiterima) + `NO`) AS Total
		FROM
		tb_pmb_tr_camaba AS t
		LEFT OUTER JOIN (
		SELECT
		c.Tahun_Masuk,
		count(c.IsDiterima) AS NO
		FROM
		tb_pmb_tr_camaba AS c
		WHERE
		c.IsDiterima = 'NO'
		GROUP BY
		c.Tahun_Masuk
		) AS l ON t.Tahun_Masuk = l.Tahun_Masuk
		WHERE
		t.IsDiterima = 'YES'
		GROUP BY
		t.Tahun_Masuk";
		echo $query;
		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data
			);
	}
	//Detailmahasiswa Daftar Ulang
	function akd_form_mhs_daftar($tahun){
		$header = $this->db->get(DB_AKD.'tb_pmb_tr_camaba')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Tahun_Masuk,$row->Nama_Mhs);
			$sql_header[] = " MAX(CASE WHEN Tahun_Masuk  = '".$row->Tahun_Masuk."' THEN Jml_Nilai END) Jml_".$initial;
		}
		$sql_header = implode(',',$sql_header);

		$sql = "
		SELECT
		a.Id_Camaba,
		a.NRP,
		a.Nama_Mhs,
		a.JK,
		a.Tlp_HP,
		a.Email,
		a.Alamat_asl,
		a.Kode_Kota_asl,
		a.Tgl_Daftar,
		a.Kode_Prodi,
		a.Tahun_Masuk,
		a.Semester_Masuk
		FROM
		tb_pmb_tr_camaba a
		WHERE
		Tahun_Masuk = '".$tahun."'";

		$data = $this->db->query($sql)->result();
		return (object)array(
			// 'header' => $header,
			'data' => $data
			);
	}

	function getDosenMK($jabatan){
		$query = "SELECT * FROM view_mk_dosen WHERE ikatan_kerja = '$jabatan'";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getSebaranIPK($prodi){
		$header = array(
			array('id_sebaran' => 'Seb-1', 'rentang' => '< 2.75'),
			array('id_sebaran' => 'Seb-2', 'rentang' => '2.75 - 3'),
			array('id_sebaran' => 'Seb-3', 'rentang' => '> 3')
		);
		if ($prodi != 'Semua Prodi'){
			$include_prodi = ', m.Kode_Prodi';
			$include_where = " AND m.Kode_Prodi = '$prodi'";
			$include_join = array(' AND l.Kode_Prodi = m.Kode_Prodi', ' AND m.Kode_Prodi = m.Kode_Prodi');
		}
		$query = "SELECT n.Tahun, n.Periode_Sem$include_prodi,
		IFNULL(COUNT(n.IPK), 0) as Seb_1, IFNULL(l.Seb_2, 0) AS Seb_2, IFNULL(p.Seb_3, 0) as Seb_3
		FROM tb_akd_tr_statistik_nilai n
		INNER JOIN tb_akd_rf_mahasiswa m ON m.NRP=n.NRP
		LEFT OUTER JOIN (SELECT n.Tahun, n.Periode_Sem$include_prodi, COUNT(n.IPK) as Seb_2
		FROM tb_akd_tr_statistik_nilai n
		WHERE n.IPK is not null AND n.IPK BETWEEN 2.75 AND 3$include_where
		GROUP BY n.Tahun, n.Periode_Sem$include_prodi)
		as l on (l.Tahun = n.Tahun AND l.Periode_Sem = n.Periode_Sem$include_join[0])
		LEFT OUTER JOIN (SELECT n.Tahun, n.Periode_Sem$include_prodi,
		COUNT(n.IPK) as Seb_3
		FROM tb_akd_tr_statistik_nilai n
		INNER JOIN tb_akd_rf_mahasiswa m ON m.NRP=n.NRP
		WHERE n.IPK is not null AND n.IPK BETWEEN 3 AND 4$include_where
		GROUP BY n.Tahun, n.Periode_Sem$include_prodi)
		as p on (p.Tahun = n.Tahun AND p.Periode_Sem = n.Periode_Sem$include_join[1])
		WHERE
		n.IPK is not null AND n.IPK BETWEEN 0 AND 2.75$include_where
		GROUP BY n.Tahun, n.Periode_Sem$include_prodi";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data,
			'header'=>$header
		);
	}
	//Jumlah Mahasiswa Daftar
	function getMhsDaftar()
	{
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi,$row->Nama_MK);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_Nilai END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "
		SELECT
		Tahun,
		$sql_header,
		SUM(Jml_Nilai) as Total
		FROM
		(
		SELECT
		Tahun_Masuk AS Tahun,
		Kode_Prodi,
		COUNT(*) AS Jml_Nilai
		FROM tb_pmb_tr_camaba
		GROUP BY Tahun_Masuk
		ORDER By Tahun_Masuk DESC
		) as main
		GROUP BY
		Tahun
		ORDER BY
		Tahun DESC";

		$data = $this->db->query($query)->result();
		echo $query;
		return (object)array(
			'data' => $data,
			'header'=>$header
		);
	}
	//Jumlah Mahasiswa Daftar Ulang
	function getMhsDaftarUlang()
	{

		$query = "SELECT Tahun_Penerimaan AS Tahun, COUNT(*) AS Total
		FROM tb_pmb_tr_camaba_reg
		GROUP BY Tahun_Penerimaan
		ORDER BY Tahun ASC
		";

		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data
			);
	}
	//USED FOR FORM AKD KELAS SP
	function akd_form_kelas_SP($id){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$query = "
		SELECT
		a.NRP,
		a.Nama_Mhs,
		b.Kode_MK,
		b.Nilai,
		c.Nama_MK,
		c.Kode_Prodi,
		b.Tahun,
		b.Periode_Sem
		FROM
		tb_akd_rf_mahasiswa as a
		INNER JOIN tb_akd_tr_nilai b ON a.NRP = b.NRP
		INNER JOIN tb_akd_rf_mata_kuliah c ON b.Kode_MK = c.Kode_MK
		WHERE
		b.Kode_MK = '$id' and a.Status_Akademis='A'
		GROUP BY b.Tahun,b.Periode_Sem
		HAVING
		b.Nilai='C+'or
		b.Nilai = 'C' OR
		b.Nilai = 'D' OR
		b.Nilai = 'E'
		ORDER BY  b.Tahun DESC, b.Periode_Sem DESC
		";

		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
			);
	}
	//used for : G_AKD_KELAS_SP
	function getALLG_AKD_KELAS_SP(){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi,$row->Nama_MK);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_Nilai END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);


		$query = "
		SELECT
		a.Kode_MK,
		c.NRP,
		b.Nama_MK,
		a.Tahun,
		a.Periode_Sem,
		c.Kode_Prodi,
		c.Status_Akademis,
		COUNT(c.NRP) as Total
		FROM
		tb_akd_tr_nilai a
		INNER JOIN tb_akd_rf_mata_kuliah b ON a.Kode_MK = b.Kode_MK
		INNER JOIN tb_akd_rf_mahasiswa c ON a.NRP = c.NRP
		WHERE
		a.Nilai='C+' OR a.Nilai='C' OR a.Nilai='D' OR a.Nilai='E'
		GROUP BY a.Kode_MK
		HAVING
		C.Status_Akademis='A'
		ORDER BY
		Tahun DESC, Periode_Sem DESC , NRP Desc
		LIMIT 20
		"
		;

		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
			);

	}
	//used for akd_form_kesiapan_tugas_akhir
	function getform_akd_kesiapan_tugas_akhir($id){
		$query2 = "SELECT
		m.NRP,
		m.Nama_Mhs,
		mk.Tahun,
		n.Periode_Sem,
		n.Num_Semester,
		n.IPK,
		n.SKS_Lulus,
		n.SKS_Keseluruhan,
		n.SKS_Sekarang,
		p.Periode_Sem,
		m.Kode_Prodi
		FROM
		tb_akd_rf_mahasiswa m, tb_akd_tr_statistik_nilai n,
		tb_akd_tr_ambil_mk mk,tb_akd_tr_perwalian p
		WHERE
		m.NRP = n.NRP AND
		n.NRP = p.NRP AND
		mk.Kd_Perwalian = p.Kd_perwalian
		and	m.nrp='$id'
		ORDER BY n.Tahun DESC
		limit 1";
		$query = "SELECT
		m.NRP,
		m.Nama_Mhs,
		m.Kode_Prodi,
		n.Kode_MK,
		n.Nilai,
		tb_akd_rf_mata_kuliah.Nama_MK,
		n.Tahun,
		n.Periode_Sem,
		n.Ambil_Ke,
		n.Status_Nilai,
		n.Kelas,
		n.Total_Nilai
		FROM
		tb_akd_rf_mahasiswa AS m
		INNER JOIN tb_akd_tr_nilai AS n ON n.NRP = m.NRP
		INNER JOIN tb_akd_rf_mata_kuliah ON n.Kode_MK = tb_akd_rf_mata_kuliah.Kode_MK
		WHERE
		m.NRP='$id'
		HAVING
		n.Nilai='C'OR n.Nilai='D' OR n.Nilai='E'
		ORDER BY n.Tahun DESC
		";
		$data2 = $this->db->query($query2)->result();
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data,
			'data2'=>$data2
			);
	}
	// Used for akd_kesiapan_tugas_akhirs
	function getakd_kesiapan_tugas_akhir(){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi,$row->Nama_MK);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_Nilai END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);


		$query = "SELECT
		m.NRP,
		m.Nama_Mhs,
		mk.Tahun,
		n.Periode_Sem,
		n.Num_Semester,
		n.IPK,
		n.SKS_Lulus,
		n.SKS_Keseluruhan,
		n.SKS_Sekarang,
		p.Periode_Sem,
		m.Kode_Prodi
		FROM
		tb_akd_rf_mahasiswa m, tb_akd_tr_statistik_nilai n,
		tb_akd_tr_ambil_mk mk,tb_akd_tr_perwalian p
		WHERE
		m.Status_Akademis = 'A' and
		m.NRP = n.NRP AND
		n.NRP = p.NRP AND
		mk.Kd_Perwalian = p.Kd_perwalian
		GROUP BY Nama_Mhs
		ORDER BY mk.Tahun DESC  , n.Num_Semester DESC";

		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
			);

	}
	//used for : AKD_KELAS_SP
	function getALLSPdata(){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi,$row->Nama_MK);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_Nilai END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "SELECT
		Kode_MK,
		Tahun,
		Periode_Sem,
		Nama_MK,
		$sql_header,
		SUM(Jml_Nilai) as Total
		FROM
		(
		SELECT
		b.Kode_MK,
		c.Nama_MK,
		c.Kode_Prodi,
		b.Tahun,
		b.Periode_Sem,
		b.Nilai,
		a.Status_Akademis,
		COUNT(b.NRP) as Jml_Nilai
		FROM
		tb_akd_rf_mahasiswa as a
		INNER JOIN tb_akd_tr_nilai b ON a.NRP = b.NRP
		INNER JOIN tb_akd_rf_mata_kuliah c ON b.Kode_MK = c.Kode_MK
		WHERE
		a.Status_Akademis='A'
		GROUP BY b.Tahun, b.Periode_Sem
		HAVING
		b.Nilai='C+' OR
		b.Nilai = 'C' OR
		b.Nilai = 'D' OR
		b.Nilai = 'E'
		ORDER BY  b.Tahun DESC, b.Periode_Sem DESC
		) as main
		GROUP BY
		Tahun, Periode_Sem
		ORDER BY
		Tahun DESC, Periode_Sem DESC
		"
		;

		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
			);

	}
	function getAllData()
	{
		$query = "SELECT
		m.NRP,
		Nama_Mhs as Nama,
		Tahun_Masuk as Angkatan,
		IF(JK='L', 'Laki-Laki', 'Perempuan') as Jenis_Kelamin,
		k.Kelas_Deskripsi as Kelas,
		Nama_Prodi as Prodi,
		p.Jenjang,
		s.`Status`,
		ip.IPK,
		Kota_asl as Kota_Asal
		FROM
		".DB_AKD."tb_akd_rf_mahasiswa m
		INNER JOIN
		".DB_AKD."tb_akd_rf_prodi p ON m.Kode_Prodi=p.Kode_Prodi
		INNER JOIN
		".DB_AKD."tb_akd_rf_status_akademis s ON s.Kode_Status=m.Status_Akademis
		INNER JOIN
		".DB_AKD."tb_akd_rf_kelas_mhs k ON k.Kelas_Mhs=m.Kelas
		LEFT JOIN
		(
		SELECT
		*
		FROM
		(
		SELECT
		NRP, IF(IPK = 0, null, IPK) as IPK
		FROM
		".DB_AKD."tb_akd_tr_statistik_nilai
		ORDER BY
		Tahun DESC, Periode_Sem DESC
		) as main
		GROUP BY
		NRP
		) as ip ON ip.NRP = m.NRP
		ORDER BY
		NRP ASC
		";
		return $this->db->query($query)->result();
	}
	//Used by : laporan/akd_ip_periode
	function getRataIPPerPeriode()
	{
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Rata_IP END) Rata_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "SELECT
		Tahun,
		Periode_Sem,
		$sql_header,
		AVG(Rata_IP) as Total
		FROM
		(
		SELECT
		n.Tahun,
		n.Periode_Sem,
		m.Kode_Prodi,
		AVG(IP) as Rata_IP,
		COUNT(IP) as Jml_IP,
		SUM(IP) as Tot_IP
		FROM
		".DB_AKD."tb_akd_tr_statistik_nilai n
		INNER JOIN
		".DB_AKD."tb_akd_rf_mahasiswa m ON m.NRP=n.NRP
		WHERE
		n.IP is not null
		GROUP BY
		n.Tahun, n.Periode_Sem, m.Kode_Prodi
		) as main
		GROUP BY
		Tahun, Periode_Sem
		ORDER BY
		Tahun DESC, Periode_Sem DESC";

		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data,
			);

	}
	function getIPK3PerPeriode($min, $max)
	{
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_IPK END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "SELECT
		Tahun,
		Periode_Sem,
		$sql_header,
		SUM(Jml_IPK) as Total
		FROM
		(
		SELECT
		n.Tahun,
		n.Periode_Sem,
		m.Kode_Prodi,
		COUNT(IPK) as Jml_IPK,
		SUM(IPK) as Tot_IPK
		FROM
		".DB_AKD."tb_akd_tr_statistik_nilai n
		INNER JOIN
		".DB_AKD."tb_akd_rf_mahasiswa m ON m.NRP=n.NRP
		WHERE
		n.IPK is not null
		AND n.IPK BETWEEN $min AND $max
		GROUP BY
		n.Tahun, n.Periode_Sem, m.Kode_Prodi
		) as main
		GROUP BY
		Tahun, Periode_Sem
		ORDER BY
		Tahun DESC, Periode_Sem DESC";

		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data,
			);

	}
	//Used by : laporan/akd_perwalian_periode
	function getPerwalianPerPeriode()
	{
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Jml_Perwalian END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "SELECT
		Tahun,
		Periode_Sem,
		$sql_header,
		SUM(Jml_Perwalian) as Total
		FROM
		(
		SELECT
		Tahun, Periode_Sem, m.Kode_Prodi, count(*) as Jml_Perwalian
		FROM
		".DB_AKD."tb_akd_tr_perwalian p
		INNER JOIN
		".DB_AKD."tb_akd_rf_mahasiswa m ON p.NRP=m.NRP
		WHERE
		IsValidate = 'YES'
		GROUP BY
		Tahun, Periode_Sem, m.Kode_Prodi
		ORDER BY
		Tahun DESC, Periode_Sem DESC
		) as main
		GROUP BY
		Tahun, Periode_Sem
		ORDER BY
		Tahun DESC, Periode_Sem DESC";

		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data,
		);

	}
	function getJabatan()
	{
		$query = "SELECT
		jabatan_akademik as Jabatan,
		SUM(Jml_Jabatan) as Total
		FROM
		(
		SELECT
		jabatan_akademik, d.Kode_Prodi, count(*) as Jml_Jabatan
		FROM
		tb_akd_tr_dosen d
		INNER JOIN
		tb_peg_rf_pegawai p ON d.NIP=p.NIP
		GROUP BY
		jabatan_akademik
		ORDER BY
		jabatan_akademik
		) as main
		GROUP BY
		jabatan_akademik
		ORDER BY
		jabatan_akademik ASC";

		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data,
		);
	}
	//function akd_form_jadwal_dosen(){
	function akd_form_jadwal_dosen($nip, $tahun, $periode){
				$header = $this->db->get(DB_AKD.'tb_akd_tr_dosen')->result();
		$sql = "SELECT
		tb_akd_rf_mata_kuliah.Kode_MK,
		tb_akd_rf_mata_kuliah.Nama_MK,
		tb_akd_rf_mata_kuliah.sks,
		COUNT(*) as total_kelas
		FROM
		tb_akd_rf_mata_kuliah ,
		tb_peg_rf_pegawai ,
		tb_akd_tr_jadwal ,
		tb_akd_tr_dosen
		WHERE tb_akd_tr_jadwal.NIP = tb_akd_tr_dosen.NIP AND tb_akd_tr_jadwal.Kode_MK = tb_akd_rf_mata_kuliah.Kode_MK
		AND tb_peg_rf_pegawai.nip = tb_akd_tr_dosen.NIP AND tb_akd_rf_mata_kuliah.Kode_MK = tb_akd_tr_jadwal.Kode_MK
		AND tb_akd_tr_jadwal.NIP = ".$nip." AND tb_akd_tr_jadwal.Tahun = $tahun AND tb_akd_tr_jadwal.Periode_Sem = '$periode'
		GROUP BY
		tb_akd_tr_jadwal.Kode_MK
		";
				echo $query;
				$data = $this->db->query($sql)->result();
				return (object)array(
					'header' => $header,
					'data' => $data);
	}

	function getJadwalKelasByPeriode($tahun, $periode){
		$query = "SELECT nip, nama, total_jadwal, beban_ajar FROM
		(SELECT p.nip, p.nama, Count(j.Kode_Jadwal) AS total_jadwal,
		Sum(mk.sks) AS beban_ajar
		FROM tb_peg_rf_pegawai AS p
		INNER JOIN tb_akd_tr_dosen AS d ON d.NIP = p.nip
		INNER JOIN tb_akd_tr_jadwal AS j ON j.NIP = d.NIP
		INNER JOIN tb_akd_rf_mata_kuliah AS mk ON j.Kode_MK = mk.Kode_MK
		WHERE j.Tahun = '$tahun' AND j.Periode_Sem = '$periode'
		GROUP BY p.nama
		ORDER BY total_jadwal DESC) AS dosen ORDER BY nama";
		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data,
		);
	}

	function getPeriodeJadwal(){
		$query = "SELECT DISTINCT CONCAT_WS(' ', Tahun, Periode_Sem) as periode FROM tb_akd_tr_jadwal ORDER BY Tahun, Periode_Sem ASC";
		$data = $this->db->query($query)->result();
		return $data;
	}

	function getJadwalKelas() {
		$query = "SELECT nip, nama, total_jadwal, beban_ajar FROM
		(SELECT p.nip, p.nama, Count(j.Kode_Jadwal) AS total_jadwal,
		Sum(mk.sks) AS beban_ajar
		FROM tb_peg_rf_pegawai AS p
		INNER JOIN tb_akd_tr_dosen AS d ON d.NIP = p.nip
		INNER JOIN tb_akd_tr_jadwal AS j ON j.NIP = d.NIP
		INNER JOIN tb_akd_rf_mata_kuliah AS mk ON j.Kode_MK = mk.Kode_MK
		GROUP BY p.nama
		ORDER BY total_jadwal DESC) AS dosen ORDER BY nama";
		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data,
		);
	}
	function getIkatanKerjaDosen(){
		$query = "SELECT b.ikatan_kerja AS Ikatan_Kerja, COUNT(b.`ikatan_kerja`) AS Total
		FROM tb_akd_tr_jadwal a, tb_akd_tr_dosen b
		WHERE b.NIP = a.NIP
		GROUP BY b.`ikatan_kerja`";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
			);
	}
	function getIkatanKerjaDosen2()
	{
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi = '".$row->Kode_Prodi."' THEN Total END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT
		Ikatan_Kerja,
		$sql_header,
		SUM(Total) as Total_Ikatan
		FROM
		(
		SELECT b.ikatan_kerja AS Ikatan_Kerja, COUNT(b.`ikatan_kerja`) as Total, b.Kode_Prodi
		FROM tb_akd_tr_jadwal a, tb_akd_tr_dosen b
		WHERE b.NIP = a.NIP
		GROUP BY b.`ikatan_kerja`, b.Kode_Prodi) as main GROUP BY Ikatan_Kerja;";

		echo $query;
		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data,
			);

	}
	function getMatkulUlang()
	{
		$query = "SELECT
		d.Nama_MK AS Matkul,
		b.Kode_MK AS Kode_Matkul,
		Max(b.Pengambilan_Ke) AS Pengambilan,
		dos.NIP,
		peg.nama
		FROM
		tb_akd_tr_ambil_mk AS b,
		tb_akd_rf_mata_kuliah AS d,
		tb_akd_tr_dosen AS dos,
		tb_akd_tr_jadwal AS jad,
		tb_peg_rf_pegawai AS peg
		WHERE
		d.Kode_MK = b.Kode_MK
		AND b.Pengambilan_Ke > 1
		AND dos.NIP = peg.nip
		AND dos.NIP = jad.NIP
		AND b.Kode_MK = jad.Kode_MK
		GROUP BY
		b.`Kode_MK`,
		d.`Nama_MK`
		ORDER BY
		MAX(b.`Pengambilan_Ke`) DESC
		LIMIT 10";

		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data
			);
	}

	function getMhsPeriode() {
		$header = $this->db->get(DB_AKD.'tb_akd_rf_status_akademis')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace(' ','_', $row->Status);
			$sql_header[] = "MAX(CASE WHEN Status  = '".$row->Status."' THEN Jml_Mhs END) Jml_".$initial;
		}
		$sql_header = implode(',', $sql_header);

		$query = "SELECT
		Tahun,
		Periode_Sem,
		$sql_header,
		SUM(Jml_Mhs) as Total
		FROM
		(
		SELECT
		Tahun, Periode_Sem, a.Status, count(*) as Jml_Mhs
		FROM
		".DB_AKD."tb_akd_tr_status_mahasiswa m
		INNER JOIN
		".DB_AKD."tb_akd_rf_status_akademis a ON m.Kode_Status=a.Kode_Status
		GROUP BY
		Tahun, Periode_Sem, a.Status
		ORDER BY
		Tahun DESC, Periode_Sem DESC
		) as main
		GROUP BY
		Tahun, Periode_Sem
		ORDER BY
		Tahun DESC, Periode_Sem DESC";
		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data,
			);

	}

	function getMhsKota() {
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$initial = str_replace('-','_', $row->Kode_Prodi);
			$sql_header[] = " MAX(CASE WHEN Kode_Prodi  = '".$row->Kode_Prodi."' THEN Total END) Total_".$initial;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT Kota, $sql_header, SUM(Total) AS Total
		FROM (SELECT a.Kota_asl AS Kota, a.Kode_Prodi, a.Tempat_Lahir AS Tempat_Lahir,COUNT(*) AS Total
		FROM tb_akd_rf_mahasiswa a
		GROUP BY a.Kota_asl) as main
		GROUP BY Kota";
		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getMahasiswaDaftar(){
		$query = "SELECT d.Tahun_Masuk, COUNT(*) AS Jml_Daftar, s.Seleksi, p.Prestasi,
			a.Diterima, t.Ditolak, du.Daftar_Ulang FROM tb_pmb_tr_camaba AS d
			LEFT OUTER JOIN view_mhs_seleksi AS s ON s.Tahun_Masuk = d.Tahun_Masuk
			LEFT OUTER JOIN view_mhs_jalur_prestasi AS p ON p.Tahun_Masuk = d.Tahun_Masuk
			LEFT OUTER JOIN view_mhs_diterima AS a ON a.Tahun_Masuk = d.Tahun_Masuk
			LEFT OUTER JOIN view_mhs_tidak_diterima AS t ON t.Tahun_Masuk = d.Tahun_Masuk
			LEFT OUTER JOIN view_mhs_daftar_ulang as du ON du.Tahun = d.Tahun_Masuk
			GROUP BY d.Tahun_Masuk ORDER By d.Tahun_Masuk DESC";
		$data = $this->db->query($query)->result();
		return (object) array(
			'data' => $data
		);
	}

	function getMatkulSP() {
		$query = "SELECT
		b.Kode_MK AS Kode_Matkul,
		a.Nama_MK AS Matkul,
		b.Periode_Sem AS Periode,
		b.Tahun AS Tahun,
		COUNT(b.Kode_MK) AS Total
		FROM
		tb_akd_rf_mata_kuliah a,
		tb_akd_tr_ambil_mk b
		WHERE
		b.Kode_MK = a.Kode_MK
		AND b.Periode_Sem IN (
		'Pendek_Genap',
		'Pendek_Ganjil'
		)
		GROUP BY
		b.Tahun
		ORDER BY
		b.Tahun,
		Total DESC";
		$data = $this->db->query($query)->result();

		return (object)array(
			'data' => $data
			);
	}

	function getCamabaKota($Prop){
		$header = $this->db->query('SELECT DISTINCT Tahun_Masuk FROM tb_pmb_tr_camaba ORDER BY Tahun_Masuk ASC')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$sql_header[] = " MAX(CASE WHEN Tahun_Masuk = '".$row->Tahun_Masuk."' THEN Jumlah_Kota END) Total_".$row->Tahun_Masuk;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT Nama_Kota, $sql_header, SUM(Jumlah_Kota) Total FROM view_kota_asal_camaba where Nama_Prop = '$Prop' GROUP BY Nama_Kota";
		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getCamabaProp(){
		$header = $this->db->query('SELECT DISTINCT Tahun_Masuk FROM tb_pmb_tr_camaba ORDER BY Tahun_Masuk ASC')->result();
		if (!$header) return false;
		$sql_header = array();
		foreach($header as $row)
		{
			$sql_header[] = " MAX(CASE WHEN Tahun_Masuk = '".$row->Tahun_Masuk."' THEN Jumlah_Prop END) Total_".$row->Tahun_Masuk;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT Nama_Prop, $sql_header, SUM(Jumlah_Prop) Total FROM view_prop_asal_camaba GROUP BY Nama_Prop";
		$data = $this->db->query($query)->result();

		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getDosenTetapKeahlianPS(){
		$query = "SELECT P.nama, D.nidn, P.tgl_lahir, D.jabatan_akademik, D.pendidikan_tertinggi,
		P.gelar_depan, P.gelar_belakang FROM tb_peg_rf_pegawai AS P
		INNER JOIN tb_akd_tr_dosen AS D ON D.NIP = P.nip WHERE D.ikatan_kerja = 'Dosen Tetap PT'";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getAllMhsRegulerLulusan(){
		$query = "SELECT M.Tahun, M.Jumlah_Semua, M.Jumlah_Maba, M.Jumlah_Aktif, M.Jumlah_Lulus,
			R.Min, R.Rat, R.Max, (I.Seb_1) Persen_1, (I.Seb_2) Persen_2,
			(I.Seb_3) Persen_3
			FROM (SELECT Tahun, SUM(Jumlah) as Jumlah_Semua,
			IFNULL(MAX(CASE WHEN Kode_Status = 'MB' THEN Jumlah END), 0) Jumlah_Maba,
			IFNULL(MAX(CASE WHEN Kode_Status = 'A' THEN Jumlah END), 0) Jumlah_Aktif,
			IFNULL(MAX(CASE WHEN Kode_Status = 'L' THEN Jumlah END), 0) Jumlah_Lulus
			FROM (SELECT S.Tahun, S.Kode_Status, Count(S.NRP) as Jumlah
			FROM tb_akd_tr_status_mahasiswa AS S
			GROUP BY S.Tahun, S.Kode_Status) as main GROUP BY Tahun) as M
			INNER JOIN (SELECT N.Tahun, Min(N.IPK) AS Min, Avg(N.IPK) AS Rat, Max(N.IPK) AS Max
			FROM tb_akd_tr_statistik_nilai AS N
			INNER JOIN tb_akd_tr_status_mahasiswa AS S ON N.NRP = S.NRP
			WHERE S.Kode_Status = 'L' GROUP BY N.Tahun) AS R ON R.Tahun = M.Tahun
			INNER JOIN (SELECT n.Tahun, IFNULL(COUNT(n.IPK), 0) as Seb_1, IFNULL(l.Seb_2, 0) AS Seb_2,
			IFNULL(p.Seb_3, 0) as Seb_3, (COUNT(n.IPK) + IFNULL(l.Seb_2, 0) + IFNULL(p.Seb_3, 0)) as Total
			FROM tb_akd_tr_statistik_nilai n
			INNER JOIN tb_akd_tr_status_mahasiswa s ON n.NRP = s.NRP
			INNER JOIN tb_akd_rf_mahasiswa m ON s.NRP = m.NRP
			INNER JOIN (SELECT n.Tahun, COUNT(n.IPK) as Seb_2
			FROM tb_akd_tr_statistik_nilai n
			INNER JOIN tb_akd_tr_status_mahasiswa s ON n.NRP = s.NRP
			INNER JOIN tb_akd_rf_mahasiswa m ON s.NRP = m.NRP
			WHERE s.Kode_Status = 'L' AND n.IPK is not null AND n.IPK BETWEEN 2.75 AND 3.5
			GROUP BY n.Tahun) as l on (l.Tahun = n.Tahun)
			INNER JOIN (SELECT n.Tahun, n.Periode_Sem, COUNT(n.IPK) as Seb_3
			FROM tb_akd_tr_statistik_nilai n
			INNER JOIN tb_akd_tr_status_mahasiswa s ON n.NRP = s.NRP
			INNER JOIN tb_akd_rf_mahasiswa m ON s.NRP = m.NRP
			WHERE s.Kode_Status = 'L' AND n.IPK is not null AND n.IPK BETWEEN 3.5 AND 4
			GROUP BY n.Tahun) as p on (p.Tahun = n.Tahun)
			WHERE s.Kode_Status = 'L' AND n.IPK is not null AND n.IPK BETWEEN 0 AND 2.75
			GROUP BY n.Tahun) AS I on I.Tahun = M.Tahun GROUP BY M.Tahun
			ORDER BY M.Tahun DESC";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getJumlahMhsAngkatan($prodi = 'TI-S1', $kode_status){
		$query_tahun = "SELECT DISTINCT Tahun_Masuk FROM tb_akd_rf_mahasiswa WHERE Tahun_Masuk IS NOT NULL;";
		$header = $this->db->query($query_tahun)->result();
		if (!$header) return false;
		$sql_header = array();
		foreach ($header as $row){
			$sql_header[] = " MAX(CASE WHEN Tahun_Masuk = '".$row->Tahun_Masuk."' THEN Jumlah END) Jml_".$row->Tahun_Masuk;
		}
		$sql_header = implode(',', $sql_header);
		if (strcmp($kode_status, 'ALL')){
			$status_mhs = "S.Kode_Status = '$kode_status' AND";
		}
		$query = "SELECT Tahun, $sql_header,
		SUM(Jumlah) as Total FROM (SELECT M.Tahun_Masuk, S.Tahun, M.Kode_Prodi,
		Count(S.NRP) as Jumlah FROM tb_akd_rf_mahasiswa AS M
		INNER JOIN tb_akd_tr_status_mahasiswa AS S ON S.NRP = M.NRP
		WHERE $status_mhs M.Kode_Prodi = '$prodi' AND S.Tahun >= M.Tahun_Masuk
		GROUP BY S.Tahun, M.Tahun_Masuk, M.Kode_Prodi) as main
		GROUP BY Tahun;";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data,
		);
	}

	function getStrukturKurikulumMK(){
		$query = "SELECT M.Semester, M.Kode_Prodi, M.Kode_MK, M.Nama_MK, M.sks, M.Kurikulum_tahun
		FROM tb_akd_rf_mata_kuliah AS M ORDER BY M.Semester ASC, M.Kode_Prodi ASC";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getMhsAktifperDosenWali($tahun, $semester, $prodi){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		$query = "SELECT Tahun, Periode_Sem, Kode_Prodi, nip, nama, SUM(Jumlah) as Jumlah_Mhs,
			IFNULL(MAX(CASE WHEN Kode_Status = 'A' THEN Jumlah END), 0) Aktif,
			IFNULL(MAX(CASE WHEN Kode_Status = 'C' THEN Jumlah END), 0) Cuti,
			IFNULL(MAX(CASE WHEN Kode_Status = 'DC' THEN Jumlah END), 0) Dicutikan
			FROM (SELECT S.Tahun, S.Periode_Sem, M.Kode_Prodi, P.nip,
			P.nama, S.Kode_Status, Count(S.NRP) as Jumlah
			FROM tb_akd_rf_mahasiswa AS M
			INNER JOIN tb_akd_tr_dosen AS D ON M.Dosen_Wali = D.NIP
			INNER JOIN tb_peg_rf_pegawai AS P ON D.NIP = P.nip
			INNER JOIN tb_akd_tr_status_mahasiswa AS S ON S.NRP = M.NRP
			WHERE S.Tahun = '$tahun' AND S.Periode_Sem = '$semester' AND M.Kode_Prodi = '$prodi'
			GROUP BY S.Tahun, S.Periode_Sem, M.Kode_Prodi, P.nip, P.nama, S.Kode_Status) as main
			GROUP BY Tahun, Periode_Sem, Kode_Prodi, nip, nama";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getFormMhsPerDosenWali($tahun, $semester, $prodi, $nip){
		$query = "SELECT  M.NRP, M.Nama_Mhs, SA.`Status` FROM tb_akd_rf_mahasiswa AS  M
			INNER JOIN tb_akd_tr_status_mahasiswa AS SM ON SM.NRP = M.NRP
			INNER JOIN tb_akd_rf_status_akademis AS SA ON SM.Kode_Status = SA.Kode_Status
			WHERE SM.Tahun = '$tahun' AND SM.Periode_Sem = '$semester' AND M.Kode_Prodi = '$prodi'
			AND M.Dosen_Wali = '$nip'";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getPenilaianDosen($tahun, $semester, $prodi){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		$query = "SELECT Tahun, Periode_Sem, Kode_Prodi, Kode_MK, Nama_MK, nip, sks, Nama, Bobot_Tugas, Bobot_UTS, Bobot_UAS,
			SUM(Jumlah) as Jumlah_Mhs,
			IFNULL(MAX(CASE WHEN Nilai = 'A' THEN Jumlah END), 0) Jumlah_A,
			IFNULL(MAX(CASE WHEN Nilai = 'B+' THEN Jumlah END), 0) Jumlah_BPlus,
			IFNULL(MAX(CASE WHEN Nilai = 'B' THEN Jumlah END), 0) Jumlah_B,
			IFNULL(MAX(CASE WHEN Nilai = 'C+' THEN Jumlah END), 0) Jumlah_CPlus,
			IFNULL(MAX(CASE WHEN Nilai = 'C' THEN Jumlah END), 0) Jumlah_C,
			IFNULL(MAX(CASE WHEN Nilai = 'D' THEN Jumlah END), 0) Jumlah_D,
			IFNULL(MAX(CASE WHEN Nilai = 'E' THEN Jumlah END), 0) Jumlah_E
			FROM (SELECT N.Tahun, N.Periode_Sem, MK.Kode_Prodi, MK.Kode_MK, MK.Nama_MK,
			MK.sks, P.nip, P.nama, J.Bobot_UTS, J.Bobot_UAS, J.Bobot_Tugas, N.Nilai,
			COUNT(N.NRP) as Jumlah FROM tb_akd_rf_mata_kuliah AS MK
			INNER JOIN tb_akd_tr_jadwal AS J ON J.Kode_MK = MK.Kode_MK
			INNER JOIN tb_akd_tr_dosen AS D ON J.NIP = D.NIP
			INNER JOIN tb_peg_rf_pegawai AS P ON D.NIP = P.nip
			INNER JOIN tb_akd_tr_nilai AS N ON N.Kode_MK = MK.Kode_MK
			WHERE N.Tahun = '$tahun' AND N.Periode_Sem = '$semester' AND MK.Kode_Prodi = '$prodi'
			GROUP BY N.Tahun, N.Periode_Sem, MK.Kode_MK, MK.Nama_MK, MK.sks, P.nama, J.Bobot_UTS, J.Bobot_UAS,
			J.Bobot_Tugas, N.Nilai) AS main GROUP BY Tahun, Periode_Sem, Kode_MK, Nama_MK, sks, nama, Bobot_UTS,
			Bobot_UAS, Bobot_Tugas";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getFormPenilaianDosen($tahun, $semester, $prodi, $kodemk, $nip){
		$query = "SELECT M.NRP, M.Nama_Mhs, J.Kelas, N.Nilai FROM tb_akd_rf_mata_kuliah AS MK
			INNER JOIN tb_akd_tr_jadwal AS J ON J.Kode_MK = MK.Kode_MK
			INNER JOIN tb_akd_tr_dosen AS D ON J.NIP = D.NIP
			INNER JOIN tb_peg_rf_pegawai AS P ON D.NIP = P.nip
			INNER JOIN tb_akd_tr_nilai AS N ON N.Kode_MK = MK.Kode_MK
			INNER JOIN tb_akd_rf_mahasiswa AS M ON N.NRP = M.NRP
			WHERE N.Tahun = '$tahun' AND N.Periode_Sem = '$semester' AND MK.Kode_Prodi = '$prodi'
			AND J.Kode_MK = '$kodemk' AND J.NIP = '$nip'";
		$data = $this->db->query($query)->result();
		return (object)array(
			'data' => $data
		);
	}

	function getKehadiranDosen($tahun, $semester, $prodi){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		$query = "SELECT R.Tahun, R.Periode_Sem, MK.Kode_Prodi, MK.Kode_MK, MK.Nama_MK, J.Kelas,
			MK.sks, J.NIP, P.nama, Count(DP.nrp) AS Jumlah_Mhs
			FROM tb_akd_rf_mata_kuliah AS MK
			INNER JOIN tb_akd_tr_jadwal AS J ON J.Kode_MK = MK.Kode_MK
			INNER JOIN tb_akd_tr_dosen AS D ON J.NIP = D.NIP
			INNER JOIN tb_peg_rf_pegawai AS P ON D.NIP = P.nip
			INNER JOIN tb_skm_tr_presensi AS R ON R.KodeMK = MK.Kode_MK
			INNER JOIN tb_skm_tr_detail_presensi AS DP ON DP.id_presensi = R.id_presensi
			WHERE R.Tahun = '$tahun' AND R.Periode_Sem = '$semester' AND MK.Kode_Prodi = '$prodi'
			GROUP BY R.Tahun, R.Periode_Sem, MK.Kode_Prodi, MK.Kode_MK, MK.Nama_MK, MK.sks, P.nama";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}

	function getDetailKehadiranMhs($tahun, $semester, $prodi, $kodemk, $kelas, $nip){
		$query = "SELECT PR.Tahun, PR.Periode_Sem, M.Kode_Prodi, PR.KodeMK, PR.Kelas,
			J.NIP, PR.materi_bahasan, PR.metode, PR.pertemuan_ke, Count(DP.nrp) AS Jumlah_Hadir
			FROM tb_akd_rf_mata_kuliah AS M
			INNER JOIN tb_akd_tr_jadwal AS J ON J.Kode_MK = M.Kode_MK
			INNER JOIN tb_skm_tr_presensi AS PR ON PR.KodeMK = M.Kode_MK
			INNER JOIN tb_skm_tr_detail_presensi AS DP ON DP.id_presensi = PR.id_presensi
			WHERE PR.Tahun = '$tahun' AND PR.Periode_Sem = '$semester' AND M.Kode_Prodi = '$prodi'
			AND PR.Kelas = '$kelas' AND J.NIP = '$nip' AND DP.Id_Keterangan = 1
			GROUP BY PR.Tahun, PR.Periode_Sem, PR.KodeMK,
			PR.Kelas, J.NIP, PR.materi_bahasan, PR.metode, PR.pertemuan_ke";
		$data = $this->db->query($query);
		return $data;
	}

	function getIPKMahasiswa($tahun = '2013', $semester = 'Ganjil', $prodi = 'TI-S1'){
		$header = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		$query = "SELECT SN.Periode_Sem, SN.Tahun, M.Tahun_Masuk, M.NRP, M.Nama_Mhs, M.Kode_Prodi,
			P.nama, SN.SKS_Lulus,SN.IPK FROM tb_akd_rf_mahasiswa AS M
			INNER JOIN tb_akd_tr_statistik_nilai AS SN ON SN.NRP = M.NRP
			INNER JOIN tb_akd_tr_dosen ON M.Dosen_Wali = tb_akd_tr_dosen.NIP
			INNER JOIN tb_peg_rf_pegawai AS P ON tb_akd_tr_dosen.NIP = P.nip
			WHERE SN.Periode_Sem = '$semester' AND SN.Tahun = '$tahun' AND M.Kode_Prodi = '$prodi'
			ORDER BY SN.Tahun, SN.Periode_Sem ASC";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data
		);
	}
}
