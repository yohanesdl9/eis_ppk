<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_pegawai extends CI_MODEL
{
	function getAllData()
	{
		$query = "
		SELECT
			p.nip as NIP,
			p.nama as Nama,
			p.jenis_kelamin as Jenis_Kelamin,
			pf.profesi as Profesi,
			g.nama_gol as Golongan,
			s.status as Status,
			u.nama_unit as Unit,
			IF(p.tgl_masuk <> '1970-01-01' , FLOOR(DATEDIFF(NOW(), p.tgl_masuk)/365), null) as Lama_Kerja
		FROM
			".DB_PEG."tb_peg_rf_pegawai p
		LEFT JOIN
			".DB_PEG."tb_peg_rf_profesi pf ON p.profesi_id=pf.profesi_id
		LEFT JOIN
			".DB_PEG."tb_peg_rf_gol g ON p.gol_id=g.gol_id
		LEFT JOIN
			".DB_PEG."tb_peg_rf_status_peg s ON p.statuspeg_id = s.statuspeg_id
		LEFT JOIN
			".DB_PEG."tb_peg_rf_unit u ON p.unit_id=u.unit_id
		WHERE
			p.aktif = 'YA'
		";
		return $this->db->query($query)->result();
	}
	
}