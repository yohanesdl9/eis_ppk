<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_laporan extends CI_MODEL
{
	var $permitted = array();

	public function getSingleData($kode)
	{
		return $this->db->where('Kode_Laporan', $kode)->get(DB_EIS.'tb_eis_rf_laporan')->row();
	}
	
	public function getSingleDataByClass($class)
	{
		return $this->db->where('Class', $class)->get(DB_EIS.'tb_eis_rf_laporan')->row();
	}
	
	public function getJmlLaporanPerJenis()
	{
		$data = $this->db
		->select('count(*) as Jml, Jenis')
		->group_by('Jenis')
		->get(DB_EIS.'tb_eis_rf_laporan')
		->result();
		$data_new = array();
		if(is_array($data)) foreach($data as $row) $data_new[$row->Jenis] = $row->Jml;
		
		return $data_new;
	}
	
	
	public function getAllPermittedUser($kode = false)
	{
		$where_kode = $kode ? " AND l.Kode_Laporan = '$kode' " : "";
		$query = "SELECT
		main.*,
		l.Kode_Laporan,
		IF(l.isView is null, 'YES', l.isView) as isView,
		IF(l.isAdmin is null, 'NO', l.isAdmin) as isAdmin,
		IF(l.isNew is null, 'YES', l.isNew) as isNew,
		l.Created_By,
		l.Created_Date
		FROM
		(
		SELECT
		u.user_username as Username,
		CONCAT(p.gelar_depan, ' ', p.nama,' ',p.gelar_belakang) as Nama
		FROM
		".DB_APP."tb_app_tr_user_group ug
		INNER JOIN
		".DB_APP."tb_app_rf_user u ON ug.Username=u.user_username
		INNER JOIN
		".DB_PEG."tb_peg_rf_pegawai p ON u.NIP=p.nip
		WHERE
		ug.App_id = '".APP_ID."'
		GROUP BY
		p.nip
		) main
		LEFT JOIN
		".DB_EIS."tb_eis_tr_akses_laporan l ON main.Username=l.Username $where_kode
		GROUP BY 
		main.Username";
		
		$data = $this->db->query($query)->result();
		return $data;
	}

	public function isImPermitted($akses, $kode)
	{
		if(!isset($this->permitted[$kode]))
		{
			$query = "SELECT
			main.*,
			l.Kode_Laporan,
			IF(l.isView is null, 'YES', l.isView) as isView,
			IF(l.isAdmin is null, 'NO', l.isAdmin) as isAdmin,
			IF(l.isNew is null, 'YES', l.isNew) as isNew,
			l.Created_By,
			l.Created_Date
			FROM
			(
			SELECT
			u.user_username as Username,
			CONCAT(p.gelar_depan, ' ', p.nama,' ',p.gelar_belakang) as Nama
			FROM
			".DB_APP."tb_app_tr_user_group ug
			INNER JOIN
			".DB_APP."tb_app_rf_user u ON ug.Username=u.user_username
			INNER JOIN
			".DB_PEG."tb_peg_rf_pegawai p ON u.NIP=p.nip
			WHERE
			ug.App_id = '".APP_ID."'
			AND
			u.user_username = '".USERNAME."'
			GROUP BY
			p.nip
			) main
			LEFT JOIN
			".DB_EIS."tb_eis_tr_akses_laporan l ON main.Username=l.Username AND l.Kode_Laporan = '$kode'
			WHERE
			main.Username = '".USERNAME."'
			";
			$data = $this->db->query($query)->row();
			$this->permitted[$kode] = $data;
		}
		else
		{
			$data = $this->permitted[$kode];
		}
		
		if (!$data) return false;
		
		if($akses == 'view' && $data->isView == 'YES') return true;
		if($akses == 'admin' && $data->isAdmin == 'YES') return true;
		
		return false;
	}

	public function saveAkses($kode, $data)
	{
		if(!$this->isImPermitted('admin', $kode)) return false;
		
		if(!is_array($data)) return false;
		
		$data_baru = array();
		foreach($data as $row)
		{
			$check = $this->db->where('Kode_Laporan', $kode)->where('Username', $row['user'])->get(DB_EIS.'tb_eis_tr_akses_laporan')->row();
			
			$send = array(
				'Username' => $row['user'],
				'Kode_Laporan' => $kode,
				'isView' => isset($row['view']) ? $row['view'] : 'NO',
				'isAdmin' => isset($row['admin']) ? $row['admin'] : 'NO',
				'Created_By' => USERNAME,
				);
			
			if ($check) 
				$res = $this->db->where('Kode_Laporan', $kode)->where('Username', $row['user'])->update(DB_EIS.'tb_eis_tr_akses_laporan', $send);
			else
				$res = $this->db->insert(DB_EIS.'tb_eis_tr_akses_laporan', $send);
		}
		
		return $res;
	}

	public function setMyLaporanNotNewAnymore($kode)
	{
		$check = $this->db
		->where('Kode_Laporan', $kode)
		->where('Username', USERNAME)
		->get(DB_EIS.'tb_eis_tr_akses_laporan')
		->row();
		
		$send = array(
			'Username' => USERNAME,
			'Kode_Laporan' => $kode,
			'isView' => isset($check->isView) ? $check->isView : 'YES',
			'isAdmin' => isset($check->isAdmin) ? $check->isAdmin : 'NO',
			'isNew' => 'NO',
			'Created_By' => USERNAME,
			);
		
		if ($check) 
			$res = $this->db
		->where('Kode_Laporan', $kode)
		->where('Username', $row['user'])
		->update(DB_EIS.'tb_eis_tr_akses_laporan', $send);
		else
			$res = $this->db->insert(DB_EIS.'tb_eis_tr_akses_laporan', $send);
	}
}