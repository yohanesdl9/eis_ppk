<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_misc extends CI_MODEL
{
	function getAllProdi()
	{
		$data = $this->db->get(DB_AKD.'tb_akd_rf_prodi')->result();
		$data_new = array();
		if($data) foreach($data as $row) $data_new[$row->Kode_Prodi] = $row;
		return $data_new;
	}
	
	function getAllKelas()
	{
		$data = $this->db->get(DB_AKD.'tb_akd_rf_kelas_mhs')->result();
		$data_new = array();
		if($data) foreach($data as $row) $data_new[$row->Kelas_Mhs] = $row;
		return $data_new;
	}
	
	function getAllStatusAkademis()
	{
		$data = $this->db->get(DB_AKD.'tb_akd_rf_status_akademis')->result();
		$data_new = array();
		if($data) foreach($data as $row) $data_new[$row->Kode_Status] = $row;
		return $data_new;
	}
	
	function getAllPeriodeSmt()
	{
		$data = $this->db->select('Semester')->group_by('Semester')->get(DB_AKD.'tb_akd_rf_periode')->result();
		$data_new = array();
		if($data) foreach($data as $row) $data_new[] = $row->Semester;
		return $data_new;
	}
}