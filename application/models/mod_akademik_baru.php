<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_akademik_baru extends CI_MODEL
{
	function getDataLaporanTes(){
		$query = "SELECT * FROM tb_akd_rf_prodi";
		return $this->db->query($query)->result();
	}
	
	function getMahasiswaAktifPerTahun($prodi = 'TI-S1'){
		$query_tahun = "SELECT DISTINCT Tahun FROM tb_akd_tr_status_mahasiswa 
			WHERE Tahun >= (SELECT MIN(Tahun_Masuk) FROM tb_akd_rf_mahasiswa);";
		$header = $this->db->query($query_tahun)->result();
		if (!$header) return false;
		$sql_header = array();
		foreach ($header as $row){
			$sql_header[] = " MAX(CASE WHEN Tahun = '".$row->Tahun."' THEN Jumlah END) Jml_".$row->Tahun;
		}
		$sql_header = implode(',', $sql_header);
		$query = "SELECT Tahun_Masuk as Angkatan, $sql_header,
		SUM(Jumlah) as Total FROM
		(SELECT M.Tahun_Masuk,
		S.Tahun, M.Kode_Prodi,
		Count(S.NRP) as Jumlah
		FROM
		tb_akd_rf_mahasiswa AS M
		INNER JOIN tb_akd_tr_status_mahasiswa AS S ON S.NRP = M.NRP
		WHERE
		S.Kode_Status = 'A' AND M.Kode_Prodi = '$prodi' AND S.Tahun >= M.Tahun_Masuk
		GROUP BY
		M.Tahun_Masuk, S.Tahun, M.Kode_Prodi HAVING MIN(Tahun_Masuk)) as main
		GROUP BY Tahun_Masuk;";
		$data = $this->db->query($query)->result();
		return (object)array(
			'header' => $header,
			'data' => $data,
		);
	}
}