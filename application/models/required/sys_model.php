<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sys_Model extends MY_Model
{
    public $var=array();
    
    function __construct() {
		parent::__construct();
		session_start();
	}
    
    function _clearExpiratedSess()
	{
        $now=$this->session->_get_time();
        $sess_expiration=$this->config->item('sess_expiration');
        $sess_table_name=$this->config->item('sess_table_name');
        $expire = $now - $sess_expiration;

        $this->db->where("last_activity < {$expire}");
        $this->db->delete($sess_table_name);
    }
    
    public function sendAlert1($t){
        $alert=$this->getAlertDetail('1');
        $alert=$this->getMessage($alert,$t);
        
        $this->kirim_email($alert['sender'],$alert['recipient'],$alert['subject'],$alert['pesan']);
    }
    
	public function sendAlert2($t){
        $alert=$this->getAlertDetail('2');
        $alert=$this->getMessage($alert,$t);
        
        $this->kirim_email($alert['sender'],$alert['recipient'],$alert['subject'],$alert['pesan']);
    }
    
	public function sendAlert3($t){
        $alert=$this->getAlertDetail('3');
        $alert=$this->getMessage($alert,$t);
        
        $this->kirim_email($alert['sender'],$alert['recipient'],$alert['subject'],$alert['pesan']);
    }
	
    public function sendAlert4($t){
        $alert=$this->getAlertDetail('4');
        $alert=$this->getMessage($alert,$t);
        
        $this->kirim_email($alert['sender'],$alert['recipient'],$alert['subject'],$alert['pesan']);
    }
    
	public function getMessage($alert,$t){
        $time=$this->getTodayFormated();
        $alert['pesan']=str_replace('<{username}>',$t['username'],$alert['pesan']);
        $alert['pesan']=str_replace('<{os}>',$t['os'],$alert['pesan']);
        $alert['pesan']=str_replace('<{browser}>',$t['browser'],$alert['pesan']);
        $alert['pesan']=str_replace('<{ip}>',$t['ip'],$alert['pesan']);
        $alert['pesan']=str_replace('<{fingerprint}>',$t['fingerprint'],$alert['pesan']);
        $alert['pesan']=str_replace('<{app_id}>',APP_ID,$alert['pesan']);
        $alert['pesan']=str_replace('<{alert_code}>',$alert['id'],$alert['pesan']);
        $alert['pesan']=str_replace('<{max_failed}>',$t['max_failed'],$alert['pesan']);
        $alert['pesan']=str_replace('<{time}>',$time,$alert['pesan']);
        $alert['pesan']=str_replace('<{NUser}>',$t['NUser'],$alert['pesan']);
        $alert['pesan']=str_replace('<{mac}>',$t['mac'],$alert['pesan']);
        
        $alert['subject']=str_replace('<{username}>',$t['username'],$alert['subject']);
        $alert['subject']=str_replace('<{os}>',$t['os'],$alert['subject']);
        $alert['subject']=str_replace('<{browser}>',$t['browser'],$alert['subject']);
        $alert['subject']=str_replace('<{ip}>',$t['ip'],$alert['subject']);
        $alert['subject']=str_replace('<{fingerprint}>',$t['fingerprint'],$alert['subject']);
        $alert['subject']=str_replace('<{app_id}>',APP_ID,$alert['subject']);
        $alert['subject']=str_replace('<{alert_code}>',$alert['id'],$alert['subject']);
        $alert['subject']=str_replace('<{max_failed}>',$t['max_failed'],$alert['subject']);
        $alert['subject']=str_replace('<{time}>',$time,$alert['subject']);
        $alert['subject']=str_replace('<{NUser}>',$t['NUser'],$alert['subject']);
        $alert['subject']=str_replace('<{mac}>',$t['mac'],$alert['subject']);
        
        $alert['sender']=str_replace('<{username}>',$t['username'],$alert['sender']);
        $alert['sender']=str_replace('<{os}>',$t['os'],$alert['sender']);
        $alert['sender']=str_replace('<{browser}>',$t['browser'],$alert['sender']);
        $alert['sender']=str_replace('<{ip}>',$t['ip'],$alert['sender']);
        $alert['sender']=str_replace('<{fingerprint}>',$t['fingerprint'],$alert['sender']);
        $alert['sender']=str_replace('<{app_id}>',APP_ID,$alert['sender']);
        $alert['sender']=str_replace('<{alert_code}>',$alert['id'],$alert['sender']);
        $alert['sender']=str_replace('<{max_failed}>',$t['max_failed'],$alert['sender']);
        $alert['sender']=str_replace('<{time}>',$time,$alert['sender']);
        $alert['sender']=str_replace('<{NUser}>',$t['NUser'],$alert['sender']);
        $alert['sender']=str_replace('<{mac}>',$t['mac'],$alert['sender']);
        return $alert;
    }
    
    public function doCekLogin($u,$p,$t)
	{
        $u = $this->db->escape_str($u);
		$p = $this->db->escape_str($p);
		$ref = $_POST['ref'];
		$fid = $_POST['fid'];
		if ($ref) $ref = base64_decode($ref);
		
		$jml_failed = $this->session->userdata('failed_login');
		$last_attempt = $this->session->userdata('last_attempt');
		$selisih = strtotime("now") - $last_attempt;
		$t['username']=$u;
        $t['fingerprint']=$t['fid'];
        $t['max_failed']='3';
		if ($jml_failed >= 3 && $selisih < 300)
		{
			$this->sendAlert1($t);
			$this->session->set_flashdata('result_login', 'Anda sudah gagal login 3 kali, harap tunggu beberapa menit.');
			$this->session->set_userdata('last_attempt', strtotime("now"));
			
			//return false;
		} 
		else if ($jml_failed >= 3 && $selisih >= 300)
		{
			$this->session->unset_userdata('failed_login');
			$this->session->unset_userdata('last_attempt');
			$jml_failed = 0;
		}
		
		// token play
		$log = array(
			'user' => $u,
			'fid' => $fid,
			'ip' => $ip,
		);
		
		$token = $_POST['wtoken'];
		if ($token)
		{
			$token = json_decode(base64_decode(base64_decode($token)));
			if (is_object($token))
			{
				$log['ip'] = $token->i;
				$log['mac'] = $token->m; 
				$log['wifi_username'] = $token->u; 
			}
		}

		$query = "SELECT
            	a.user_username,
            	a.user_password,
            	a.nama AS namaNip,
            	ID,
            	a.Role_id,
            	a.Role_Name,
            	IFNULL(a.photo, '') AS photo#,
            	#a.mac_add,
            	#a.is_only
            FROM
            	(
            		SELECT
            			us.user_username,
            			user_password,
            			nama,
            			gp.Role_id,
            			Role_Name,
            			IFNULL(us.NIP,us.NRP) AS ID,
            			CONCAT(Domain, Direktori, NamaFile) AS photo#,
            			#ub.mac_add,
            			#ub.is_only
            		FROM
            			".DB_APP."tb_app_tr_user_group gp
            		INNER JOIN ".DB_APP."tb_app_rf_user us ON gp.Username = us.user_username
            		LEFT JOIN ".DB_PEG."tb_peg_rf_pegawai peg ON us.NIP = peg.nip
            		INNER JOIN ".DB_APP."tb_app_rf_group rf_gp ON rf_gp.Role_id = gp.Role_id
            		LEFT JOIN ".DB_APP."tb_app_rf_files files ON peg.IDFile = files.IDFile
                    #LEFT JOIN ".DB_APP."tb_app_tr_user_browser ub ON us.user_username=ub.user_username
            		WHERE
            			us.user_username = '$u'
            		AND user_password = md5(PASSWORD('$p'))
            		AND us.user_username = gp.username
            		AND gp.App_id = '".APP_ID."'
            		AND us.isAktif = 'YES' 
            	) a";
        $qck = $this->db->query($query)->row();

		if($qck)
		{
			if(strtoupper($qck->is_only)=='YES')
			{
				if($qck->mac_add!=$t['mac']){
					$this->session->set_flashdata('result_login', 'Anda tidak dapat membuka sistem dari komputer ini.');
					$this->sendAlert4($t);
					redirect(base_url());
				}
			}
			
			if(!$this->isRecognizedBrowser($u,$t['fingerprint'])){
				if(!empty($t['mac'])){
					if($this->isRecognizedMac($u,$t['mac'])){
						//$this->sendAlert3($t);
					}else
						$this->sendAlert2($t);
				}else
					$this->sendAlert2($t);       
			}
			$this->writeLoginLog($t);
			
			if($qck->ID='020032'){
				//do nothing
			}else{
				$query="DELETE FROM ".DB_APP."tb_app_rf_sessions WHERE user_data LIKE '%$u%' AND user_data LIKE '%login_on_app_".APP_ID."%'";
				$this->db->query($query);  
			}
			
			$sess_data['logged_in'] = APP_ID;
			$sess_data['locked'] = 'NO';
			$sess_data['username'] = $qck->user_username;
			if($qck->namaNip!=''){
				$sess_data['nama_lengkap'] = $qck->namaNip;}
			else
			{
				$sess_data['nama_lengkap'] = $qck->namaNrp;
			};
			$sess_data['role']=$qck->Role_id;
			$sess_data['login_role']='role_as_'.$qck->Role_id;
			$sess_data['login_app']='login_on_app_'.APP_ID;
			$sess_data['photo'] = $qck->photo ? $qck->photo : base_url('assets/custom/img/avatar.png');
			$sess_data['roleName']=$qck->Role_Name;
			$this->session->set_userdata($sess_data);
			$_SESSION['userdata'] = $sess_data;
			
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			$log['success'] = '1';
			
			return true;

		}
		else
		{
			$log['success'] = '0';
			$jml_failed = $jml_failed > 0 ? $jml_failed + 1 : 1;
			$this->session->set_userdata('failed_login', $jml_failed);
			$this->session->set_userdata('last_attempt', strtotime("now"));
			$this->session->set_flashdata('result_login', 'Username atau Password yang anda masukkan salah.');
            $tag=APP_ID.' LoginFailed';
            $this->writeLog($u,'login','index',"failed login attempt $jml_failed time/s using username=$u and password=$p ip=".$t['ip']." mac=".$t['mac']." network user=".$t['NUser'].' fid='.$fid,$tag,$u);
			return false;
		}
    }
    
    public function writeLoginLog($t){
        $data=array(
            "App_id"=>APP_ID,
            "User_username"=>$t['username'],
            "os"=>$t['os'],
            "browser"=>$t['browser'],
            "ip"=>$t['ip'],
            "mac"=>$t['mac'],
            "browser_fingerprint"=>$t['fid'],
            "network_user"=>$t['NUser'],
        );
        $this->db->insert(DB_APP.'tb_app_tr_login_log',$data);
    }
    
    public function getOsMarketName($os){
        $res=$this->db->query("SELECT
            	market_name
            FROM
            	".DB_APP."tb_app_rf_os
            WHERE
            	CONCAT(vendor, ' NT ', version_name) LIKE '$os'");
        $res=$res->row_array();
        return $res['market_name'];
    }
    
	public function changePassword($old, $new, $confirm)
	{
		
		if ($new != $confirm) die('0');
		
		$check = $this->db
						->where('user_username', USERNAME)
						->where('user_password', 'MD5(PASSWORD("'.$old.'"))', false)
						->get(DB_APP.'tb_app_rf_user')
						->row();
						
		if (!$check) die('1');
		
		$query="UPDATE ".DB_APP."tb_app_rf_user
				SET 
					user_password = MD5(PASSWORD('$new')),
					Modified_App = 'SIMKEU',
					Modified_By = '".USERNAME."',
					Modified_Date = now() 
				WHERE
					user_username = '".USERNAME."'";   
		$res = $this->db->query($query);
		
		if (!$res) die('2');
		
		return $this->changePasswordMikrotik(USERNAME, $new);
	}
	
	public function get_induk($induk='0')
	{
        $data = array();
		
		$query = 'SELECT
            	g.Group_id,
            	g.Menu_id,
            	m.Order_Menu,
            	m.Nama_Menu,
            	m.Induk_Menu,
            	m.URL,
            	m.Icon
            FROM
            	'.DB_APP.'tb_app_tr_group_menu g
            INNER JOIN '.DB_APP.'tb_app_rf_menus m ON g.Menu_id = m.Menu_id
            WHERE
            	g.Group_id = "'.$this->session->userdata('role').'"
                AND (m.Induk_Menu = "'.$induk.'" OR m.Induk_Menu IS NULL OR m.Induk_Menu ="") 
                AND App_id="'.APP_ID.'"
                AND m.isAktif="YES"
				AND Induk_Menu = "'.$induk.'"
            ORDER BY
            	Order_Menu ASC';
		$result = $this->db->query($query)->result();
		foreach($result as $row)
		{
			$data[] = array(
					'Menu_id'	=>$row->Menu_id,
					'Nama_Menu'	=>$row->Nama_Menu,
                    'Icon'  =>$row->Icon,
                    'URL'   =>$row->URL,
                    'child'	=>$this->get_child($row->Menu_id)										
				);
		}
		return $data;
    }
    
	public function get_child($id)
	{
		$data = array();
		$result = $this->db->query('SELECT
            	g.Group_id,
            	g.Menu_id,
            	m.Order_Menu,
            	m.Nama_Menu,
            	m.Induk_Menu,
            	m.URL,
            	m.Icon
            FROM
            	'.DB_APP.'tb_app_tr_group_menu g
            INNER JOIN '.DB_APP.'tb_app_rf_menus m ON g.Menu_id = m.Menu_id
            WHERE
            	g.Group_id = "'.$this->session->userdata('role').'"
                AND m.Induk_Menu = "'.$id.'"
                AND App_id="'.APP_ID.'"
                AND m.isAktif="YES"
            ORDER BY
            	Order_Menu ASC');

		foreach($result->result() as $row)
		{
	       $data[] = array(
					'Menu_id'	=>$row->Menu_id,
					'Nama_Menu'	=>$row->Nama_Menu,
                    'Icon'  =>$row->Icon,
                    'URL'   =>$row->URL,
					'child'	=>$this->get_child($row->Menu_id)
				);
		}
		return $data;
	}
    
	public function setSubApp($page_id)
	{
		$data = $this->getPageAttr($page_id);
		if ($data) 
		{
			$this->session->set_userdata('sub_app', $data->Menu_id);
		}
		
		return $data;
	
	}
	
	public function getPageAttr($page_id)
	{
		return $this->db
					->where('URL', "/$page_id")
					->where('App_id', APP_ID)
					->get(DB_APP.'tb_app_rf_menus')
					->row();
	}
	
    public function getPageAttribut($classname)
	{
        $res = $this->db->query("SELECT Nama_Menu, Icon FROM ".DB_APP."tb_app_rf_menus WHERE URL='/$classname' AND App_id='".APP_ID."'")->row();
		return $res ? $res : 'Unauthorized';
    }
    
    public function getBreadCrumb($classname)
	{
		$sub_app = $this->session->userdata('sub_app');
        $appId=APP_ID;
        $res=$this->db->query("SELECT Menu_id,Nama_Menu,URL,Icon,Induk_Menu FROM ".DB_APP."tb_app_rf_menus WHERE URL='/$classname' AND App_id='$appId' and Menu_id <> '$sub_app'");
        $res=$res->result_array();
        $res=$res[0];
        return $res;
    }
    
    public function getBreadCrumbByMenuId($id)
	{
		$sub_app = $this->session->userdata('sub_app');
        $appId=APP_ID;
        $res=$this->db->query("SELECT Menu_id,Nama_Menu,URL,Icon,Induk_Menu FROM ".DB_APP."tb_app_rf_menus WHERE Menu_id='$id' AND App_id='$appId' and Menu_id <> '$sub_app'");
        $res=$res->result_array();
        $res=$res[0];
        return $res;
    }
	
	function mikrotik_encrypt($str){
        $metode=$this->getConfigItem('mikrotik_encryption');
        $metode=str_replace('<{str}>',$str,$metode);
        $res=$this->db->query("SELECT $metode AS pwd");
        $res=$res->row_array();
        return $res['pwd'];
    }
	
}