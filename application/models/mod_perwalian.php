<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_perwalian extends CI_MODEL
{
	function getAllData()
	{
		$query = "SELECT 
			p.NRP,
			ms.Nama_Mhs,
			IF(JK='L', 'Laki-Laki', 'Perempuan') as Jenis_Kelamin,
			k.Kelas_Deskripsi as Kelas,
			Nama_Prodi as Prodi,
			ps.Jenjang,
			s.`Status`,
			p.Tahun,
			p.Periode_Sem as Semester,
			IF(IsValidate = 'YES', 'Ya', 'Tidak') as Sudah_Validasi,
			sum(m.sks) as Jml_SKS
		FROM 
			".DB_AKD."tb_akd_tr_perwalian p
		INNER JOIN
			".DB_AKD."tb_akd_rf_mahasiswa ms on p.NRP=ms.NRP
		INNER JOIN 
			".DB_AKD."tb_akd_rf_prodi ps ON ms.Kode_Prodi=ps.Kode_Prodi
		INNER JOIN
			".DB_AKD."tb_akd_rf_status_akademis s ON s.Kode_Status=ms.Status_Akademis
		INNER JOIN
			".DB_AKD."tb_akd_rf_kelas_mhs k ON k.Kelas_Mhs=ms.Kelas
		LEFT JOIN
			".DB_AKD."tb_akd_tr_ambil_mk a ON a.Kd_Perwalian = p.Kd_perwalian
		LEFT JOIN
			".DB_AKD."tb_akd_rf_mata_kuliah m ON a.Kode_MK=m.Kode_MK
		WHERE
			a.Kode_DropMK is null OR a.Kd_Perwalian = ''
		GROUP BY
			p.Kd_perwalian
		";
		return $this->db->query($query)->result();
	}


}