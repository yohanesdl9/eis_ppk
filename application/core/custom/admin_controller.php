<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_controller extends MY_Controller{

	function __construct() 
	{
		parent::__construct();
		$exept = array('katalog');
		
		if (in_array($this->uri->segment(1), $exept))
		{
			define('PAGE_ID', $this->uri->segment(1).'/'.$this->uri->segment(2));
		}
		else
		{
			define('PAGE_ID', $this->uri->segment(1));
		}
		
		$menu = trim($this->session->userdata('menu'));
		if (!$menu || strlen($menu) <= 130) $this->session->set_userdata('menu', generateMenu(USERNAME));
		
		if((!$this->session->userdata('logged_in') || $this->session->userdata('locked') != 'NO') && !isset($_SESSION['userdata']['logged_in'])) 
		{
			$referrer = str_replace('=','',base64_encode($_SERVER[REQUEST_URI]));
			session_destroy();
			redirect(base_url('?ref='.$referrer));
		} 
		else if(!$this->session->userdata('logged_in') || $this->session->userdata('locked') != 'NO' && $_SESSION['userdata']['logged_in'])
		{
			$this->session->set_userdata( $_SESSION['userdata'] );
		}

	}

}
?>