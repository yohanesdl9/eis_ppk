<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_Controller extends Admin_Controller{

	var $data;
	
	function __construct() 
	{
		parent::__construct();
		
		$curr_class = get_class($this);
		$data = $this->mod_laporan->getSingleDataByClass($curr_class);
		if(!$data) show_404();
		
		define('PAGE_TITLE', $data->Deskripsi);
		define('PAGE_CODE', $data->Kode_Laporan);
		define('PAGE_TYPE', $data->Jenis);
		define('PAGE_ID_LAP', PAGE_ID.'/view/'.PAGE_CODE);
		
		if (!$this->mod_laporan->isImPermitted('view', PAGE_CODE)) show_404();
		
		if ($this->mod_laporan->permitted[PAGE_CODE]->isNew == 'YES') $this->mod_laporan->setMyLaporanNotNewAnymore(PAGE_CODE);
		
		$this->data = $data;
	}


}
?>