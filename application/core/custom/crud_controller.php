<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crud_controller extends admin_controller{

	public $title = 'Nama';
	public $column = array
	(
		'id' => array(
			'title' => 'ID',
			'width' => '10%',
			'filter' => 'text',
			'attribut' => '{"class": "text-center"}',
		), 
		'nama_gol' => array(
			'title' => 'Nama Golongan',
			'filter' => 'text',
		), 
		'options-no-db' => array(
			'title' => 'Option',
			'width' => '10%',
			'attribut' => '{"sortable": false, "class": "text-center"}',
		)
	);
	public $model = false;
	public $table = 'tes';
	public $primary_key = 'id';
	public $form_data = array(
		'nama' => array(
			'title' => 'Nama User',
			'type' => 'text',
		),
	);
	
	function __construct() 
	{
		parent::__construct();
		
		if (!$this->model)
		{
			$this->load->model('crud_model');
			$this->crud_model->set($this->table, $this->primary_key);
			$this->model = 'crud_model';
		}
		else
		{
			$this->load->model($this->model);
		}		
	}
	
	public function index()
	{
		$this->load->template('crud/v_main');
	}
	
	function get_datatable()
	{
		$this->load->library('datatable');
		$kolom = array_keys($this->column);
		$filter = $kolom;
		$i = 0;
		foreach($this->column as $col) 
		{
			if (isset($col['filter_field'])) $filter[$i] = $col['filter_field'];
			$i++;
		}
		
		$kolom = array(
			'field' => $kolom,
			'filter' => $filter,
		);
		
		$data = $this->datatable->render($kolom, $this->table, $this->primary_key);
		echo json_encode($data);
	}
	
	public function form($id=false)
	{
	
		$model = $this->model;
		
		if ($this->input->server('REQUEST_METHOD') == 'POST') 
		{
			$data = array();
			
			foreach ($this->form_data as $key => $val)
			{
				$data[$key] = $_POST[$key];
			}			
			$res = $this->$model->save($data, $id);
			die('ok');
		}
		

		$pack = array(
			'data' => ($id) ? $this->$model->get_detail_data($id) : false,
			'id' => $id,
		);
		
		$this->load->view('crud/v_form', $pack);
	
	}
	
	public function detail($id)
	{
		$model = $this->model;
		$pack = array(
			'data' => $this->$model->get_detail_data($id),
			'id' => $id,
		);
		$this->load->view('crud/v_detail', $pack);
	
	}
	
	public function delete($id)
	{
		$model = $this->model;
		$res = $this->$model->delete($id);
		if ($res) echo 'ok';
	}
	
}
?>