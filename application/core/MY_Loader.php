<?php

class MY_Loader extends CI_Loader {
	
    public function template($template_name, $data = array(), $return = false )
    {
		$CI =& get_instance();
		
		$page_attribut = $CI->sys_model->getPageAttribut(PAGE_ID);
		
		define('PAGE_TITLE', isset($page_attribut->Nama_Menu) ? $page_attribut->Nama_Menu : $page_attribut );
		define('PAGE_ICON', isset($page_attribut->Icon) ? $page_attribut->Icon : '' );
		
		$data['breadcrumb'] = generateBreadcrumb(PAGE_ID);
		$data['content'] = $this->view($template_name, $data, true);
		
		$template = $this->parseContent($this->view('template/main', $data, true));
		die($template);
    }
	
	private function parseContent($content)
	{
		$matches = array();
		$section = array();
		$search = array();
		$replace = array();
		
		preg_match_all('/\[section name=\"(.+?)\"\](.+?)\[\/section\]/s', $content, $matches);
		
		if (is_array($matches[1])) foreach($matches[1] as $i => $row)
		{
			$section[$row] = $matches[2][$i];
			$search[] = '/\[section=\"'.$row.'\"(.*)\]/';
			$replace[] = $matches[2][$i];
		}
		
		$content = preg_replace('/\[section (.+?)\[\/section\]/s', '', $content);
		$content = preg_replace($search, $replace, $content);
		$content = preg_replace('/\[section(.*)default\=\"(.*)\"(.*)\]/', '$2', $content);
		$content = preg_replace('/\[section(.*)\]/', '', $content);
		return $content;
	}
}