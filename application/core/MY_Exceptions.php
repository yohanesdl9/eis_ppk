<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

    public function show_404($page = '', $log_error = true)
    {
        $CI =& get_instance();
		
		// if ajax
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			echo $CI->load->view('template/404', array('is_ajax' => true), true);
		}
		else
		{
			$CI->load->template('template/404', array('is_ajax' => false));
		}

    }
}