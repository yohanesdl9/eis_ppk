<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	function __construct() 
	{
		header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

		parent::__construct();
		
		// Define constant
		$arr = (array) $this->db;
		foreach($arr as $key => $val) if(strstr($key, 'db_')) define(strtoupper($key), $val.'.');
		
		define('USERNAME', $this->session->userdata('username'));
		define('NAMA_LENGKAP', $this->session->userdata('nama_lengkap'));
		define('ROLE', $this->session->userdata('role'));
		define('NAMA_ROLE', $this->session->userdata('roleName'));
		define('APP_ID', $this->config->item('application_id'));
		define('PHOTO', $this->session->userdata('photo'));
		
	} 
}
require_once APPPATH.'core/custom/admin_controller.php';
require_once APPPATH.'core/custom/laporan_controller.php';
require_once APPPATH.'core/custom/crud_controller.php';

?>