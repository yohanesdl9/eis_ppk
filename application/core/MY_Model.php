<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	
	function __construct() 
{
	parent::__construct();
	
	// convert setting DB
	$arr = (array) $this->db;
	foreach($arr as $key => $val) if(strstr($key, 'db_')) define(strtoupper($key), $val.'.');
}
	
	function getConfigItem($key){
			$res=$this->db->query("SELECT `value` FROM ".DB_APP."tb_app_rf_config WHERE conf_name='$key'");
			$res=$res->row_array();
			if(isset($res['value'])) return $res['value']; else return false;
	}
	
	public function addInsertLog($data)
{
			$data['Created_App']= APP_ID;
			$data['Created_by']= USERNAME;
			$data['Created_date']=$this->getTodayAsString();
			return $data;
	}
	
public function addUpdateLog($data)
{
			$data['Modified_App']= APP_ID;
			$data['Modified_by']= USERNAME;
			$data['Modified_date']=$this->getTodayAsString();
			return $data;
	}
	
public function getTodayAsString()
{
	$res = $this->db->query('SELECT now() as today');
			$res = $res->result_array();
			
			$today='';
			if (count($res)!=0){
					$today = $res[0]['today'];   
			}
			return $today;
}

	public function getTodayFormated($format='%d %M %Y jam %H:%i:%s')
{
	$res = $this->db->query("SELECT CONCAT(DAYNAME(NOW()),', ',DATE_FORMAT(NOW(),'$format')) as today");
			$res = $res->result_array();
			
			$today='';
			if (count($res)!=0){
					$today = $res[0]['today'];   
			}
			return $today;
}
	
public function writeLog($username,$className,$functionName,$message,$tags,$related)
{
			$app=APP_ID;
			$this->db->query("INSERT INTO ".DB_APP."`tb_app_tr_log` (
								`Username`,
								`ClassName`,
								`FunctionName`,
								`Message`,
								`Tags`,
									`App_id`,
								`RelatedTo`
							)
							VALUES
								(
									'$username',
									'$className',
									'$functionName',
									'$message',
									'$tags',
											'$app',
									'$related'
								);");
			return false;
	}
	
	public function kirim_email($pengirim,$ke,$subyek,$pesan) 
{
			return false;
		 $result=false;
		 $this->load->library('email');
		 $this->email->initialize(array(
					 'protocol' => $this->config->item('email_protocol'),
					 'smtp_host' => $this->config->item('email_smtp_host'),
					 'smtp_user' => $this->config->item('email_smtp_user'),
					 'smtp_pass' => $this->config->item('email_smtp_pass'),
					 'smtp_port' => $this->config->item('email_smtp_port'),
					 'mailtype' => $this->config->item('email_mailtype'),
					 'charset'  => $this->config->item('email_charset'),
					 'newline' => $this->config->item('email_newline') // kode yang harus di tulis pada konfigurasi controler email
		 ));
	
		 $from = $this->config->item('email_smtp_user');
		 if(empty($pengirim)) $pengirim=$this->config->item('email_smtp_user');
		 $nama = $pengirim;
		 $to = $ke;
		 $subject = $subyek;
		 $message = $pesan;
		 $this->email->from($from, $nama )
								 ->to($to)
								 ->subject($subject)
								 ->message($message);
	
		 if ($this->email->send()) {
				//$this->session->set_flashdata('success', 'Email berhasil dikirim.');
				$result = 'sukses';
		 } else {
				show_error($this->email->print_debugger());
				$result = 'gagal';
		 }
		 return $result;
	}  
	
	public function getAlertDetail($id)
{
			$res=$this->db->query("SELECT DISTINCT
						GROUP_CONCAT((
							IFNULL(peg.email, mhs.Email)
						)) AS email,
						secAlrt.sender,
						secAlrt.`subject`,
						secAlrt.pesan,
							secAlrt.id
					FROM
					".DB_APP."tb_app_rf_security_alert secAlrt
					INNER JOIN ".DB_APP."tb_app_rf_security_alert_recipient alrt ON secAlrt.id=alrt.id_alert
					INNER JOIN ".DB_APP."tb_app_rf_user usr ON alrt.user_username = usr.user_username
					LEFT JOIN ".DB_PEG."tb_peg_rf_pegawai peg ON usr.NIP = peg.nip
					LEFT JOIN ".DB_AKD."tb_akd_rf_mahasiswa mhs ON usr.NRP = mhs.NRP
					WHERE
						alrt.id_alert = '$id'");
			$result=$res->row_array();
			$result['recipient']=explode(',',$result['email']);
			return $result;
	}
	
	public function isBrowserBanned($id)
{
			$res=$this->db->query("SELECT
						browser_fingerprint
					FROM
						".DB_APP."tb_app_tr_banned_browser
					WHERE
						browser_fingerprint = '$id'");
			if($res->num_rows()>0) return true; else return false;
	}

	public function isRecognizedBrowser($user,$finger)
{
			$res=$this->db->query("SELECT user_username FROM ".DB_APP."tb_app_tr_user_browser WHERE user_username='$user' AND browser_fingerprint='$finger'");
			if($res->num_rows()>0) return true; else return false;
	}

	public function isRecognizedMac($user,$mac)
{
			$res=$this->db->query("SELECT user_username FROM ".DB_APP."tb_app_tr_user_browser WHERE user_username='$user' AND mac_add='$mac'");
			if($res->num_rows()>0) return true; else return false;
	}
}